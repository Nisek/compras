using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Progra5Compras
{
    public partial class LOGIN : Form
    {
        public LOGIN()
        {
            InitializeComponent();
        }

        private void Label1_Click(object sender, EventArgs e)
        {

        }

        private void LOGIN_Load(object sender, EventArgs e)
        {

        }

        private void Btn_salir_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void Btn_ingresar_Click(object sender, EventArgs e)
        {
            //Primero se valida que los campos de usuario y contraseña tengan algo escrito 
            //luego se comprueba que ese usuario y contraseña tenga cuenta en el sistema.

            if (ValidarUsuario_Paso1())
            {
                Globales.Cls_Globales.MiUsuarioGlobal = new Logica.Usuario();

                Globales.Cls_Globales.MiUsuarioGlobal.UsuarioNombre = txt_usuario.Text.Trim();
                Globales.Cls_Globales.MiUsuarioGlobal.Contra = txt_password.Text.Trim();

                //Una vez cargados los atributos de UsuarioNombre y Contraseña se puede
                //consultar a la base de datos que sean válidos.

                Logica.Usuario MiUsuarioValidado = new Logica.Usuario();
                MiUsuarioValidado = Globales.Cls_Globales.MiUsuarioGlobal.ValidarUsuario();


                if (MiUsuarioValidado != null && MiUsuarioValidado.Id_usuario > 0)
                {
                    //A este punto tengo certeza del 100% que el usuario se validó correctamente

                    Globales.Cls_Globales.MiUsuarioGlobal = MiUsuarioValidado;
                    Globales.Cls_Globales.miFormPrincipal.Show();
                    this.Hide();

                }
                else
                {
                    MessageBox.Show("Usuario o Contraseña Incorrectos!", ":(", MessageBoxButtons.OK);
                    txt_password.Focus();
                    txt_password.SelectAll();
                }
            }

            
        }

        private bool ValidarUsuario_Paso1()
        {
            bool R = false;

            if (txt_usuario.Text.Trim() != string.Empty && txt_password.Text.Trim() != string.Empty)
            {
                R = true;
            }

            return R;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Globales.Cls_Globales.MiUsuarioGlobal = new Logica.Usuario();

            Globales.Cls_Globales.MiUsuarioGlobal.UsuarioNombre = "nisek";
            Globales.Cls_Globales.MiUsuarioGlobal.Contra = "1234";

            Logica.Usuario MiUsuarioValidado = new Logica.Usuario();

            MiUsuarioValidado = Globales.Cls_Globales.MiUsuarioGlobal.ValidarUsuario();

            if (MiUsuarioValidado != null && MiUsuarioValidado.Id_usuario > 0)
            {
                //A este punto tengo certeza del 100% que el usuario se validó correctamente

                Globales.Cls_Globales.MiUsuarioGlobal = MiUsuarioValidado;

                Globales.Cls_Globales.miFormPrincipal.Show();
                this.Hide();
            }
        }

        private void txt_password_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
