namespace Progra5Compras.Formularios
{
    partial class Frm_Usuarios_Gestion
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        /// 


        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.Dgv_Listado = new System.Windows.Forms.DataGridView();
            this.C_Id_usuario = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.c_Nombre = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.C_Cedula = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.C_UsuarioNombre = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.C_Contra = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.C_Email = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.C_Eliminado = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.C_Id_usuarioTipo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.C_TipoUsuario = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label8 = new System.Windows.Forms.Label();
            this.Cbox_TipoUsuario = new System.Windows.Forms.ComboBox();
            this.Btn_VerContrasenna = new System.Windows.Forms.Button();
            this.Cbox_Eliminado = new System.Windows.Forms.CheckBox();
            this.Txt_Email = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.Txt_Contrasenna = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.Txt_NombreUsuario = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.Txt_cedula = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.Txt_Nombre = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.Txt_Id_Usuario = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btn_Agregar = new System.Windows.Forms.Button();
            this.btn_modificar = new System.Windows.Forms.Button();
            this.btn_limpiar = new System.Windows.Forms.Button();
            this.Btn_Eliminar = new System.Windows.Forms.Button();
            this.btn_cerrar = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.Txt_Buscar = new System.Windows.Forms.TextBox();
            this.Cbox_VerEliminados = new System.Windows.Forms.CheckBox();
            this.Tmr_Buscar = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.Dgv_Listado)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // Dgv_Listado
            // 
            this.Dgv_Listado.AllowUserToAddRows = false;
            this.Dgv_Listado.AllowUserToDeleteRows = false;
            this.Dgv_Listado.AllowUserToOrderColumns = true;
            this.Dgv_Listado.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.Dgv_Listado.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.C_Id_usuario,
            this.c_Nombre,
            this.C_Cedula,
            this.C_UsuarioNombre,
            this.C_Contra,
            this.C_Email,
            this.C_Eliminado,
            this.C_Id_usuarioTipo,
            this.C_TipoUsuario});
            this.Dgv_Listado.Location = new System.Drawing.Point(16, 54);
            this.Dgv_Listado.Margin = new System.Windows.Forms.Padding(4);
            this.Dgv_Listado.Name = "Dgv_Listado";
            this.Dgv_Listado.ReadOnly = true;
            this.Dgv_Listado.RowHeadersVisible = false;
            this.Dgv_Listado.RowHeadersWidth = 51;
            this.Dgv_Listado.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.Dgv_Listado.Size = new System.Drawing.Size(1042, 211);
            this.Dgv_Listado.TabIndex = 0;
            this.Dgv_Listado.VirtualMode = true;
            this.Dgv_Listado.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.Dgv_Listado_CellClick);
            this.Dgv_Listado.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.Dgv_Listado_DataBindingComplete);
            // 
            // C_Id_usuario
            // 
            this.C_Id_usuario.DataPropertyName = "Id_usuario";
            this.C_Id_usuario.HeaderText = "Codigo";
            this.C_Id_usuario.MinimumWidth = 6;
            this.C_Id_usuario.Name = "C_Id_usuario";
            this.C_Id_usuario.ReadOnly = true;
            this.C_Id_usuario.Width = 125;
            // 
            // c_Nombre
            // 
            this.c_Nombre.DataPropertyName = "Nombre";
            this.c_Nombre.HeaderText = "Nombre";
            this.c_Nombre.MinimumWidth = 6;
            this.c_Nombre.Name = "c_Nombre";
            this.c_Nombre.ReadOnly = true;
            this.c_Nombre.Width = 125;
            // 
            // C_Cedula
            // 
            this.C_Cedula.DataPropertyName = "Cedula";
            this.C_Cedula.HeaderText = "Cédula";
            this.C_Cedula.MinimumWidth = 6;
            this.C_Cedula.Name = "C_Cedula";
            this.C_Cedula.ReadOnly = true;
            this.C_Cedula.Width = 125;
            // 
            // C_UsuarioNombre
            // 
            this.C_UsuarioNombre.DataPropertyName = "UsuarioNombre";
            this.C_UsuarioNombre.HeaderText = "User Name";
            this.C_UsuarioNombre.MinimumWidth = 6;
            this.C_UsuarioNombre.Name = "C_UsuarioNombre";
            this.C_UsuarioNombre.ReadOnly = true;
            this.C_UsuarioNombre.Width = 125;
            // 
            // C_Contra
            // 
            this.C_Contra.DataPropertyName = "Contra";
            this.C_Contra.HeaderText = "Contraseña";
            this.C_Contra.MinimumWidth = 6;
            this.C_Contra.Name = "C_Contra";
            this.C_Contra.ReadOnly = true;
            this.C_Contra.Visible = false;
            this.C_Contra.Width = 125;
            // 
            // C_Email
            // 
            this.C_Email.DataPropertyName = "Email";
            this.C_Email.HeaderText = "Email";
            this.C_Email.MinimumWidth = 6;
            this.C_Email.Name = "C_Email";
            this.C_Email.ReadOnly = true;
            this.C_Email.Width = 125;
            // 
            // C_Eliminado
            // 
            this.C_Eliminado.DataPropertyName = "Eliminado";
            this.C_Eliminado.HeaderText = "Eliminado";
            this.C_Eliminado.MinimumWidth = 6;
            this.C_Eliminado.Name = "C_Eliminado";
            this.C_Eliminado.ReadOnly = true;
            this.C_Eliminado.Width = 125;
            // 
            // C_Id_usuarioTipo
            // 
            this.C_Id_usuarioTipo.DataPropertyName = "Id_usuarioTipo";
            this.C_Id_usuarioTipo.HeaderText = "IDTipo";
            this.C_Id_usuarioTipo.MinimumWidth = 6;
            this.C_Id_usuarioTipo.Name = "C_Id_usuarioTipo";
            this.C_Id_usuarioTipo.ReadOnly = true;
            this.C_Id_usuarioTipo.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.C_Id_usuarioTipo.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.C_Id_usuarioTipo.Width = 125;
            // 
            // C_TipoUsuario
            // 
            this.C_TipoUsuario.DataPropertyName = "TipoUsuario";
            this.C_TipoUsuario.HeaderText = "Tipo Usuario";
            this.C_TipoUsuario.MinimumWidth = 6;
            this.C_TipoUsuario.Name = "C_TipoUsuario";
            this.C_TipoUsuario.ReadOnly = true;
            this.C_TipoUsuario.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.C_TipoUsuario.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.C_TipoUsuario.Width = 125;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.Cbox_TipoUsuario);
            this.groupBox1.Controls.Add(this.Btn_VerContrasenna);
            this.groupBox1.Controls.Add(this.Cbox_Eliminado);
            this.groupBox1.Controls.Add(this.Txt_Email);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.Txt_Contrasenna);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.Txt_NombreUsuario);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.Txt_cedula);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.Txt_Nombre);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.Txt_Id_Usuario);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Location = new System.Drawing.Point(16, 291);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox1.Size = new System.Drawing.Size(1060, 270);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Detalle de Usuario";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(656, 208);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(186, 29);
            this.label8.TabIndex = 15;
            this.label8.Text = "Tipo de Usuario";
            // 
            // Cbox_TipoUsuario
            // 
            this.Cbox_TipoUsuario.FormattingEnabled = true;
            this.Cbox_TipoUsuario.Location = new System.Drawing.Point(863, 208);
            this.Cbox_TipoUsuario.Name = "Cbox_TipoUsuario";
            this.Cbox_TipoUsuario.Size = new System.Drawing.Size(154, 24);
            this.Cbox_TipoUsuario.TabIndex = 14;
            this.Cbox_TipoUsuario.SelectedIndexChanged += new System.EventHandler(this.Cbox_TipoUsuario_SelectedIndexChanged);
            // 
            // Btn_VerContrasenna
            // 
            this.Btn_VerContrasenna.Location = new System.Drawing.Point(1002, 89);
            this.Btn_VerContrasenna.Margin = new System.Windows.Forms.Padding(4);
            this.Btn_VerContrasenna.Name = "Btn_VerContrasenna";
            this.Btn_VerContrasenna.Size = new System.Drawing.Size(51, 25);
            this.Btn_VerContrasenna.TabIndex = 13;
            this.Btn_VerContrasenna.Text = "Ver";
            this.Btn_VerContrasenna.UseVisualStyleBackColor = true;
            this.Btn_VerContrasenna.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Btn_VerContrasenna_MouseDown);
            this.Btn_VerContrasenna.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Btn_VerContrasenna_MouseUp);
            // 
            // Cbox_Eliminado
            // 
            this.Cbox_Eliminado.AutoSize = true;
            this.Cbox_Eliminado.Enabled = false;
            this.Cbox_Eliminado.Location = new System.Drawing.Point(907, 140);
            this.Cbox_Eliminado.Margin = new System.Windows.Forms.Padding(4);
            this.Cbox_Eliminado.Name = "Cbox_Eliminado";
            this.Cbox_Eliminado.Size = new System.Drawing.Size(91, 21);
            this.Cbox_Eliminado.TabIndex = 12;
            this.Cbox_Eliminado.Text = "Eliminado";
            this.Cbox_Eliminado.UseVisualStyleBackColor = true;
            // 
            // Txt_Email
            // 
            this.Txt_Email.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Email.Location = new System.Drawing.Point(109, 174);
            this.Txt_Email.Margin = new System.Windows.Forms.Padding(4);
            this.Txt_Email.Name = "Txt_Email";
            this.Txt_Email.Size = new System.Drawing.Size(356, 34);
            this.Txt_Email.TabIndex = 11;
            this.Txt_Email.TextChanged += new System.EventHandler(this.Txt_Email_TextChanged);
            this.Txt_Email.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Txt_Email_KeyPress);
            this.Txt_Email.Leave += new System.EventHandler(this.Txt_Email_Leave);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(25, 174);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(74, 29);
            this.label7.TabIndex = 10;
            this.label7.Text = "Email";
            // 
            // Txt_Contrasenna
            // 
            this.Txt_Contrasenna.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Contrasenna.Location = new System.Drawing.Point(698, 84);
            this.Txt_Contrasenna.Margin = new System.Windows.Forms.Padding(4);
            this.Txt_Contrasenna.Name = "Txt_Contrasenna";
            this.Txt_Contrasenna.Size = new System.Drawing.Size(295, 34);
            this.Txt_Contrasenna.TabIndex = 9;
            this.Txt_Contrasenna.UseSystemPasswordChar = true;
            this.Txt_Contrasenna.TextChanged += new System.EventHandler(this.Txt_Contrasenna_TextChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(548, 84);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(136, 29);
            this.label6.TabIndex = 8;
            this.label6.Text = "Contraseña";
            // 
            // Txt_NombreUsuario
            // 
            this.Txt_NombreUsuario.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_NombreUsuario.Location = new System.Drawing.Point(797, 38);
            this.Txt_NombreUsuario.Margin = new System.Windows.Forms.Padding(4);
            this.Txt_NombreUsuario.Name = "Txt_NombreUsuario";
            this.Txt_NombreUsuario.Size = new System.Drawing.Size(203, 34);
            this.Txt_NombreUsuario.TabIndex = 7;
            this.Txt_NombreUsuario.TextChanged += new System.EventHandler(this.Txt_NombreUsuario_TextChanged);
            this.Txt_NombreUsuario.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Txt_NombreUsuario_KeyPress);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(555, 38);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(224, 29);
            this.label5.TabIndex = 6;
            this.label5.Text = "Nombre de Usuario";
            // 
            // Txt_cedula
            // 
            this.Txt_cedula.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_cedula.Location = new System.Drawing.Point(112, 123);
            this.Txt_cedula.Margin = new System.Windows.Forms.Padding(4);
            this.Txt_cedula.Name = "Txt_cedula";
            this.Txt_cedula.Size = new System.Drawing.Size(284, 34);
            this.Txt_cedula.TabIndex = 5;
            this.Txt_cedula.TextChanged += new System.EventHandler(this.Txt_cedula_TextChanged);
            this.Txt_cedula.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Txt_cedula_KeyPress);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(9, 118);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(90, 29);
            this.label4.TabIndex = 4;
            this.label4.Text = "Cédula";
            // 
            // Txt_Nombre
            // 
            this.Txt_Nombre.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Nombre.Location = new System.Drawing.Point(112, 75);
            this.Txt_Nombre.Margin = new System.Windows.Forms.Padding(4);
            this.Txt_Nombre.Name = "Txt_Nombre";
            this.Txt_Nombre.Size = new System.Drawing.Size(284, 34);
            this.Txt_Nombre.TabIndex = 3;
            this.Txt_Nombre.TextChanged += new System.EventHandler(this.Txt_Nombre_TextChanged);
            this.Txt_Nombre.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Txt_Nombre_KeyPress);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(9, 78);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(101, 29);
            this.label3.TabIndex = 2;
            this.label3.Text = "Nombre";
            // 
            // Txt_Id_Usuario
            // 
            this.Txt_Id_Usuario.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Id_Usuario.Location = new System.Drawing.Point(112, 32);
            this.Txt_Id_Usuario.Margin = new System.Windows.Forms.Padding(4);
            this.Txt_Id_Usuario.Name = "Txt_Id_Usuario";
            this.Txt_Id_Usuario.ReadOnly = true;
            this.Txt_Id_Usuario.Size = new System.Drawing.Size(284, 34);
            this.Txt_Id_Usuario.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(9, 38);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(92, 29);
            this.label2.TabIndex = 0;
            this.label2.Text = "Código";
            // 
            // btn_Agregar
            // 
            this.btn_Agregar.BackColor = System.Drawing.SystemColors.Highlight;
            this.btn_Agregar.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Agregar.ForeColor = System.Drawing.Color.White;
            this.btn_Agregar.Location = new System.Drawing.Point(16, 569);
            this.btn_Agregar.Margin = new System.Windows.Forms.Padding(4);
            this.btn_Agregar.Name = "btn_Agregar";
            this.btn_Agregar.Size = new System.Drawing.Size(100, 70);
            this.btn_Agregar.TabIndex = 2;
            this.btn_Agregar.Text = "Agregar";
            this.btn_Agregar.UseVisualStyleBackColor = false;
            this.btn_Agregar.Click += new System.EventHandler(this.btn_Agregar_Click);
            // 
            // btn_modificar
            // 
            this.btn_modificar.BackColor = System.Drawing.Color.OrangeRed;
            this.btn_modificar.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_modificar.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.btn_modificar.Location = new System.Drawing.Point(157, 569);
            this.btn_modificar.Margin = new System.Windows.Forms.Padding(4);
            this.btn_modificar.Name = "btn_modificar";
            this.btn_modificar.Size = new System.Drawing.Size(109, 70);
            this.btn_modificar.TabIndex = 3;
            this.btn_modificar.Text = "Modificar";
            this.btn_modificar.UseVisualStyleBackColor = false;
            this.btn_modificar.Click += new System.EventHandler(this.btn_modificar_Click);
            // 
            // btn_limpiar
            // 
            this.btn_limpiar.BackColor = System.Drawing.Color.Teal;
            this.btn_limpiar.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_limpiar.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.btn_limpiar.Location = new System.Drawing.Point(304, 569);
            this.btn_limpiar.Margin = new System.Windows.Forms.Padding(4);
            this.btn_limpiar.Name = "btn_limpiar";
            this.btn_limpiar.Size = new System.Drawing.Size(100, 70);
            this.btn_limpiar.TabIndex = 4;
            this.btn_limpiar.Text = "Limpiar";
            this.btn_limpiar.UseVisualStyleBackColor = false;
            this.btn_limpiar.Click += new System.EventHandler(this.btn_limpiar_Click);
            // 
            // Btn_Eliminar
            // 
            this.Btn_Eliminar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.Btn_Eliminar.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Btn_Eliminar.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.Btn_Eliminar.Location = new System.Drawing.Point(449, 569);
            this.Btn_Eliminar.Margin = new System.Windows.Forms.Padding(4);
            this.Btn_Eliminar.Name = "Btn_Eliminar";
            this.Btn_Eliminar.Size = new System.Drawing.Size(100, 70);
            this.Btn_Eliminar.TabIndex = 5;
            this.Btn_Eliminar.Text = "Eliminar";
            this.Btn_Eliminar.UseVisualStyleBackColor = false;
            this.Btn_Eliminar.Click += new System.EventHandler(this.Btn_Eliminar_Click);
            // 
            // btn_cerrar
            // 
            this.btn_cerrar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.btn_cerrar.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_cerrar.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.btn_cerrar.Location = new System.Drawing.Point(965, 574);
            this.btn_cerrar.Margin = new System.Windows.Forms.Padding(4);
            this.btn_cerrar.Name = "btn_cerrar";
            this.btn_cerrar.Size = new System.Drawing.Size(100, 70);
            this.btn_cerrar.TabIndex = 6;
            this.btn_cerrar.Text = "Cerrar";
            this.btn_cerrar.UseVisualStyleBackColor = false;
            this.btn_cerrar.Click += new System.EventHandler(this.Btn_cerrar_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(16, 11);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(52, 17);
            this.label1.TabIndex = 7;
            this.label1.Text = "Buscar";
            // 
            // Txt_Buscar
            // 
            this.Txt_Buscar.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_Buscar.Location = new System.Drawing.Point(77, 6);
            this.Txt_Buscar.Margin = new System.Windows.Forms.Padding(4);
            this.Txt_Buscar.Name = "Txt_Buscar";
            this.Txt_Buscar.Size = new System.Drawing.Size(348, 30);
            this.Txt_Buscar.TabIndex = 8;
            this.Txt_Buscar.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Txt_Buscar_KeyDown);
            this.Txt_Buscar.KeyUp += new System.Windows.Forms.KeyEventHandler(this.Txt_Buscar_KeyUp);
            // 
            // Cbox_VerEliminados
            // 
            this.Cbox_VerEliminados.AutoSize = true;
            this.Cbox_VerEliminados.Location = new System.Drawing.Point(874, 10);
            this.Cbox_VerEliminados.Margin = new System.Windows.Forms.Padding(4);
            this.Cbox_VerEliminados.Name = "Cbox_VerEliminados";
            this.Cbox_VerEliminados.Size = new System.Drawing.Size(184, 21);
            this.Cbox_VerEliminados.TabIndex = 9;
            this.Cbox_VerEliminados.Text = "Ver Usuarios Eliminados";
            this.Cbox_VerEliminados.UseVisualStyleBackColor = true;
            this.Cbox_VerEliminados.Click += new System.EventHandler(this.Cbox_VerEliminados_Click);
            // 
            // Tmr_Buscar
            // 
            this.Tmr_Buscar.Interval = 1000;
            this.Tmr_Buscar.Tick += new System.EventHandler(this.Tmr_Buscar_Tick);
            // 
            // Frm_Usuarios_Gestion
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1084, 657);
            this.Controls.Add(this.Cbox_VerEliminados);
            this.Controls.Add(this.Txt_Buscar);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btn_cerrar);
            this.Controls.Add(this.Btn_Eliminar);
            this.Controls.Add(this.btn_limpiar);
            this.Controls.Add(this.btn_modificar);
            this.Controls.Add(this.btn_Agregar);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.Dgv_Listado);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "Frm_Usuarios_Gestion";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Gestion de Usuarios";
            this.Load += new System.EventHandler(this.Frm_Usuarios_Gestion_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Dgv_Listado)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView Dgv_Listado;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btn_Agregar;
        private System.Windows.Forms.Button btn_modificar;
        private System.Windows.Forms.Button btn_limpiar;
        private System.Windows.Forms.Button Btn_Eliminar;
        private System.Windows.Forms.Button btn_cerrar;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox Txt_Buscar;
        private System.Windows.Forms.CheckBox Cbox_VerEliminados;
        private System.Windows.Forms.Button Btn_VerContrasenna;
        private System.Windows.Forms.CheckBox Cbox_Eliminado;
        private System.Windows.Forms.TextBox Txt_Email;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox Txt_Contrasenna;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox Txt_NombreUsuario;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox Txt_cedula;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox Txt_Nombre;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox Txt_Id_Usuario;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox Cbox_TipoUsuario;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.DataGridViewTextBoxColumn C_Id_usuario;
        private System.Windows.Forms.DataGridViewTextBoxColumn c_Nombre;
        private System.Windows.Forms.DataGridViewTextBoxColumn C_Cedula;
        private System.Windows.Forms.DataGridViewTextBoxColumn C_UsuarioNombre;
        private System.Windows.Forms.DataGridViewTextBoxColumn C_Contra;
        private System.Windows.Forms.DataGridViewTextBoxColumn C_Email;
        private System.Windows.Forms.DataGridViewCheckBoxColumn C_Eliminado;
        private System.Windows.Forms.DataGridViewTextBoxColumn C_Id_usuarioTipo;
        private System.Windows.Forms.DataGridViewTextBoxColumn C_TipoUsuario;
        private System.Windows.Forms.Timer Tmr_Buscar;
    }
}