namespace Progra5Compras.Formularios
{
    partial class Frm_Proveedores_Buscar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txt_buscar = new System.Windows.Forms.TextBox();
            this.dgv_lista = new System.Windows.Forms.DataGridView();
            this.btn_selecionar = new System.Windows.Forms.Button();
            this.btn_cerrar = new System.Windows.Forms.Button();
            this.C_Id_proveedor = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.C_Cedula = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.C_Nombre = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.C_Telefono = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.C_Direccion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.C_Email = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.C_Eliminado = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_lista)).BeginInit();
            this.SuspendLayout();
            // 
            // txt_buscar
            // 
            this.txt_buscar.Location = new System.Drawing.Point(145, 13);
            this.txt_buscar.Name = "txt_buscar";
            this.txt_buscar.Size = new System.Drawing.Size(446, 22);
            this.txt_buscar.TabIndex = 0;
            this.txt_buscar.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // dgv_lista
            // 
            this.dgv_lista.AllowUserToAddRows = false;
            this.dgv_lista.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_lista.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.C_Id_proveedor,
            this.C_Cedula,
            this.C_Nombre,
            this.C_Telefono,
            this.C_Direccion,
            this.C_Email,
            this.C_Eliminado});
            this.dgv_lista.Location = new System.Drawing.Point(0, 41);
            this.dgv_lista.Name = "dgv_lista";
            this.dgv_lista.RowHeadersVisible = false;
            this.dgv_lista.RowHeadersWidth = 51;
            this.dgv_lista.RowTemplate.Height = 24;
            this.dgv_lista.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv_lista.Size = new System.Drawing.Size(736, 150);
            this.dgv_lista.TabIndex = 1;
            this.dgv_lista.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.dgv_lista_DataBindingComplete);
            // 
            // btn_selecionar
            // 
            this.btn_selecionar.BackColor = System.Drawing.Color.Green;
            this.btn_selecionar.ForeColor = System.Drawing.Color.White;
            this.btn_selecionar.Location = new System.Drawing.Point(76, 197);
            this.btn_selecionar.Name = "btn_selecionar";
            this.btn_selecionar.Size = new System.Drawing.Size(215, 47);
            this.btn_selecionar.TabIndex = 2;
            this.btn_selecionar.Text = "Selecionar";
            this.btn_selecionar.UseVisualStyleBackColor = false;
            this.btn_selecionar.Click += new System.EventHandler(this.btn_selecionar_Click);
            // 
            // btn_cerrar
            // 
            this.btn_cerrar.BackColor = System.Drawing.Color.DarkRed;
            this.btn_cerrar.ForeColor = System.Drawing.Color.White;
            this.btn_cerrar.Location = new System.Drawing.Point(394, 197);
            this.btn_cerrar.Name = "btn_cerrar";
            this.btn_cerrar.Size = new System.Drawing.Size(135, 47);
            this.btn_cerrar.TabIndex = 3;
            this.btn_cerrar.Text = "Cerrar";
            this.btn_cerrar.UseVisualStyleBackColor = false;
            this.btn_cerrar.Click += new System.EventHandler(this.btn_cerrar_Click);
            // 
            // C_Id_proveedor
            // 
            this.C_Id_proveedor.DataPropertyName = "Id_proveedor";
            this.C_Id_proveedor.HeaderText = "Id_proveedor";
            this.C_Id_proveedor.MinimumWidth = 6;
            this.C_Id_proveedor.Name = "C_Id_proveedor";
            this.C_Id_proveedor.Width = 125;
            // 
            // C_Cedula
            // 
            this.C_Cedula.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.C_Cedula.DataPropertyName = "Cedula";
            this.C_Cedula.HeaderText = "Cedula";
            this.C_Cedula.MinimumWidth = 6;
            this.C_Cedula.Name = "C_Cedula";
            this.C_Cedula.Width = 130;
            // 
            // C_Nombre
            // 
            this.C_Nombre.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.C_Nombre.DataPropertyName = "Nombre";
            this.C_Nombre.HeaderText = "Nombre";
            this.C_Nombre.MinimumWidth = 6;
            this.C_Nombre.Name = "C_Nombre";
            // 
            // C_Telefono
            // 
            this.C_Telefono.DataPropertyName = "Telefono";
            this.C_Telefono.HeaderText = "Telefono";
            this.C_Telefono.MinimumWidth = 6;
            this.C_Telefono.Name = "C_Telefono";
            this.C_Telefono.Visible = false;
            this.C_Telefono.Width = 125;
            // 
            // C_Direccion
            // 
            this.C_Direccion.DataPropertyName = "Direccion";
            this.C_Direccion.HeaderText = "Dirección";
            this.C_Direccion.MinimumWidth = 6;
            this.C_Direccion.Name = "C_Direccion";
            this.C_Direccion.Visible = false;
            this.C_Direccion.Width = 125;
            // 
            // C_Email
            // 
            this.C_Email.DataPropertyName = "Email";
            this.C_Email.HeaderText = "Email";
            this.C_Email.MinimumWidth = 6;
            this.C_Email.Name = "C_Email";
            this.C_Email.Visible = false;
            this.C_Email.Width = 125;
            // 
            // C_Eliminado
            // 
            this.C_Eliminado.DataPropertyName = "Eliminado";
            this.C_Eliminado.HeaderText = "Eliminado";
            this.C_Eliminado.MinimumWidth = 6;
            this.C_Eliminado.Name = "C_Eliminado";
            this.C_Eliminado.Visible = false;
            this.C_Eliminado.Width = 125;
            // 
            // Frm_Proveedores_Buscar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(748, 258);
            this.Controls.Add(this.btn_cerrar);
            this.Controls.Add(this.btn_selecionar);
            this.Controls.Add(this.dgv_lista);
            this.Controls.Add(this.txt_buscar);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "Frm_Proveedores_Buscar";
            this.Opacity = 0.85D;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Busqueda de Proveedor";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.Frm_Proveedores_Buscar_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_lista)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txt_buscar;
        private System.Windows.Forms.DataGridView dgv_lista;
        private System.Windows.Forms.Button btn_selecionar;
        private System.Windows.Forms.Button btn_cerrar;
        private System.Windows.Forms.DataGridViewTextBoxColumn C_Id_proveedor;
        private System.Windows.Forms.DataGridViewTextBoxColumn C_Cedula;
        private System.Windows.Forms.DataGridViewTextBoxColumn C_Nombre;
        private System.Windows.Forms.DataGridViewTextBoxColumn C_Telefono;
        private System.Windows.Forms.DataGridViewTextBoxColumn C_Direccion;
        private System.Windows.Forms.DataGridViewTextBoxColumn C_Email;
        private System.Windows.Forms.DataGridViewTextBoxColumn C_Eliminado;
    }
}