using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Progra5Compras.Formularios
{
    public partial class Frm_Proveedores_Buscar : Form
    {

        Logica.Proveedor ObjProveedor = new Logica.Proveedor();

        DataTable Listado = new DataTable();
        

        private void LlenarListaProveedores()
        {
            Listado = new DataTable();
            Listado = ObjProveedor.Listar();

            dgv_lista.DataSource = Listado;
        }

        public Frm_Proveedores_Buscar()
        {
            InitializeComponent();
        }

        private void dgv_lista_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            dgv_lista.ClearSelection();
        }

        private void Frm_Proveedores_Buscar_Load(object sender, EventArgs e)
        {
            LlenarListaProveedores();
        }

        private void btn_selecionar_Click(object sender, EventArgs e)
        {
            if (dgv_lista.Rows.Count > 0 && dgv_lista.SelectedRows.Count == 1)
            {
                //Una vez asegurado que se seleciono una linea se procede a modificar los atributos
                //de la composicion de proveedor del formulario de Compra.

                int IdProveedor = Convert.ToInt32(dgv_lista.SelectedRows[0].Cells["C_Id_proveedor"].Value);
                string NombreProveedor = dgv_lista.SelectedRows[0].Cells["C_Nombre"].Value.ToString();

                Globales.Cls_Globales.MiFormDeCompras.MiCompraLocal.MiProveedor.Id_Proveedor = IdProveedor;
                Globales.Cls_Globales.MiFormDeCompras.MiCompraLocal.MiProveedor.Nombre = NombreProveedor;

                this.DialogResult = DialogResult.OK;
            }
        }

        private void btn_cerrar_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }
    }
}
