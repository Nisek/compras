namespace Progra5Compras.Formularios
{
    partial class Frm_VisualizadorReportes
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Crv_Visualizador = new CrystalDecisions.Windows.Forms.CrystalReportViewer();
            this.SuspendLayout();
            // 
            // Crv_Visualizador
            // 
            this.Crv_Visualizador.ActiveViewIndex = -1;
            this.Crv_Visualizador.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Crv_Visualizador.Cursor = System.Windows.Forms.Cursors.Default;
            this.Crv_Visualizador.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Crv_Visualizador.Location = new System.Drawing.Point(0, 0);
            this.Crv_Visualizador.Name = "Crv_Visualizador";
            this.Crv_Visualizador.Size = new System.Drawing.Size(800, 450);
            this.Crv_Visualizador.TabIndex = 0;
            // 
            // Frm_VisualizadorReportes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.Crv_Visualizador);
            this.Name = "Frm_VisualizadorReportes";
            this.Text = "Reportes";
            this.ResumeLayout(false);

        }

        #endregion

        public CrystalDecisions.Windows.Forms.CrystalReportViewer Crv_Visualizador;
    }
}