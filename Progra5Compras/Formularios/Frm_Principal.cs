using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Progra5Compras.Formularios
{
    public partial class Frm_Principal : Form
    {
        public Frm_Principal()
        {
            InitializeComponent();
        }

      

        private void Frm_Principal_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void MNUUsuarios_Click(object sender, EventArgs e)
        {
            Form FormularioUsuarioGestion = new Formularios.Frm_Usuarios_Gestion();
            FormularioUsuarioGestion.Show();
        }

        private void Frm_Principal_Load(object sender, EventArgs e)
        {
            if (Globales.Cls_Globales.MiUsuarioGlobal != null && Globales.Cls_Globales.MiUsuarioGlobal.Id_usuario > 0)
            {
                int TipoUsuario = Globales.Cls_Globales.MiUsuarioGlobal.TipoUsuario.Id_usuario_tipo;

                switch (TipoUsuario)
                {
                    case 1:
                        //admin
                        //tiene acceso a todos los menús
                        break;

                    case 2:
                        MNUMantenimientos.Visible = false;
                        MNUReportes.Visible = false;
                        break;

                    case 3:
                        MNUMantenimientos.Visible = false;
                        MNUProcesos.Visible = false;
                        break;
                }
            }
        }

        private void MNURegistroCompras_Click(object sender, EventArgs e)
        {
            if (Globales.Cls_Globales.MiFormDeCompras.Visible == false)
            {
                Globales.Cls_Globales.MiFormDeCompras.Show();
            }
        }
    }
}
