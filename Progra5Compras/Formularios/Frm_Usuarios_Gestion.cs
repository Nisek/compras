using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Progra5Compras.Formularios
{
    public partial class Frm_Usuarios_Gestion : Form
{

        bool Flag_EstoyEditando = false;

        //este sera el objeto local que usaremos para administrar el 
        //usuario en el formulario
        public Logica.Usuario MiUsuarioLocal = new Logica.Usuario();


        public Frm_Usuarios_Gestion()
        {
            InitializeComponent();
        }

        private void Btn_cerrar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Frm_Usuarios_Gestion_Load(object sender, EventArgs e)
        {
            this.MdiParent = Globales.Cls_Globales.miFormPrincipal;

            LlenarListaUsuarios(true);

            LLenarComboTipoUsuario();

            ActivarAgregar();

        }

        private void LLenarComboTipoUsuario() {

            Cbox_TipoUsuario.ValueMember = "id";
            Cbox_TipoUsuario.DisplayMember = "d";

            Cbox_TipoUsuario.DataSource = MiUsuarioLocal.TipoUsuario.Listar();
            Cbox_TipoUsuario.SelectedIndex = -1;

        }

        private void LlenarListaUsuarios(bool VerActivos)
        {
            //Se Enumeran los pasos de la secuencia solo con fines
            //didácticos

            //1.1 y 1.2
            Logica.Usuario MiUsuario = new Logica.Usuario();

            //1.3 y 1.4
            DataTable Retorno;
            Retorno = MiUsuario.Listar(VerActivos);

            //1.5
            Dgv_Listado.DataSource = Retorno;

        }

        private void Dgv_Listado_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            Dgv_Listado.ClearSelection();
        }

        private void btn_Agregar_Click(object sender, EventArgs e)
        {
            if (ValidarDatosFormualrio())
            {
                MiUsuarioLocal.Nombre = Txt_Nombre.Text.Trim();
                MiUsuarioLocal.Cedula = Txt_cedula.Text.Trim();
                MiUsuarioLocal.Email = Txt_Email.Text.Trim();
                MiUsuarioLocal.UsuarioNombre = Txt_NombreUsuario.Text.Trim();
                MiUsuarioLocal.Contra = Txt_Contrasenna.Text.Trim();


                MiUsuarioLocal.TipoUsuario.Id_usuario_tipo = Convert.ToInt32(Cbox_TipoUsuario.SelectedValue);
                MiUsuarioLocal.TipoUsuario.Tipo = Cbox_TipoUsuario.SelectedText;

                //Basados en el diagrama de casos de uso extendido, se indica que
                //para poder agregar primero debemos consultarsi el nombre de usuario
                //y la cedula ya estan registrados. En el caso que NO lo esten se 
                //procede a Agregar()

                //En este caso como son dos validaciones se crean variables booleanas 
                //y luego se verifican sus valores para poder Agregar()

                bool ValidaCedula = false;
                bool ValidaUserName = false;

                ValidaCedula = MiUsuarioLocal.ConsultarPorCedula();
                ValidaUserName = MiUsuarioLocal.ConsultarPorUserName();


                if (ValidaCedula == false && ValidaUserName == false)
                {

                    MiUsuarioLocal.Agregar();
                        LlenarListaUsuarios(true);
                        LimpiarFormulario();
                    MessageBox.Show("Usuario Agregado Correctamente", ":)", MessageBoxButtons.OK);


                }
                else 
                {
                    if (ValidaCedula)
                    {
                        MessageBox.Show("Ya existe un usuario con esta Cedula!!!", ":(", MessageBoxButtons.OK);
                        Txt_cedula.Focus();
                        Txt_cedula.SelectAll();
                    }
                    if (ValidaUserName)
                    {
                        MessageBox.Show("El Nombre de usaurio no esta disponible!!!", ":(", MessageBoxButtons.OK);
                        Txt_NombreUsuario.Focus();
                        Txt_NombreUsuario.SelectAll();
                    }
                }

            }
        }
        private void LimpiarFormulario()
        {
            Txt_Id_Usuario.Clear();
            Txt_Nombre.Clear();
            Txt_cedula.Clear();
            Txt_Contrasenna.Clear();
            Txt_Email.Clear();
            Txt_NombreUsuario.Clear();
            Cbox_Eliminado.Checked = false;
            Cbox_TipoUsuario.SelectedIndex = -1;

            Flag_EstoyEditando = false;
            Txt_NombreUsuario.Enabled = true;
            
        }

        private bool ValidarDatosFormualrio(bool ValidarContrasenna = true) {

            bool R = false;


            if (Txt_Nombre.Text.Trim() != string.Empty && 
                Txt_cedula.Text.Trim() != string.Empty &&
                Txt_Email.Text.Trim() != string.Empty &&
                Txt_NombreUsuario.Text.Trim() != string.Empty &&
                Cbox_TipoUsuario.SelectedIndex >= 0)
            {
                if (ValidarContrasenna)
                {
                    if (Txt_Contrasenna.Text.Trim() != string.Empty)
                    {
                        R = true;
                    }
                }
                else
                {
                    R = true;
                }   
            }

            return R;
        }

        private void Txt_Nombre_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = Globales.Cls_Globales.ValidarCaracteresTexto(e, true);
        }

        private void Txt_cedula_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = Globales.Cls_Globales.ValidarCaracteresNumeros(e);
        }

        private void Txt_NombreUsuario_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = Globales.Cls_Globales.ValidarCaracteresTexto(e, false, true);
        }

        private void Txt_Email_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = Globales.Cls_Globales.ValidarCaracteresTexto(e, false, true);

        }

        private void Txt_Email_Leave(object sender, EventArgs e)
        {
            if (Txt_Email.Text.Trim() != string.Empty)
            {
                if (Globales.Cls_Globales.ValidarEmail(Txt_Email.Text.Trim()) == false)
                {
                    MessageBox.Show("Correo no tiene el formato correcto!!", ":(", MessageBoxButtons.OK);
                    Txt_Email.Focus();
                    Txt_Email.SelectAll();
                }
            }
        }

        private void btn_limpiar_Click(object sender, EventArgs e)
        {
            LimpiarFormulario();
            ActivarAgregar();
        }

        private void Btn_VerContrasenna_MouseDown(object sender, MouseEventArgs e)
        {
            Txt_Contrasenna.UseSystemPasswordChar = false;
        }

        private void Btn_VerContrasenna_MouseUp(object sender, MouseEventArgs e)
        {
            Txt_Contrasenna.UseSystemPasswordChar = true;
        }

        private void LimpiarUsuarioLocal()
        {
            MiUsuarioLocal.Id_usuario = 0;
            MiUsuarioLocal.Nombre = String.Empty;
            MiUsuarioLocal.Cedula = String.Empty;
            MiUsuarioLocal.Email = string.Empty;
            MiUsuarioLocal.UsuarioNombre = string.Empty;
            MiUsuarioLocal.Contra = string.Empty;
            MiUsuarioLocal.TipoUsuario.Id_usuario_tipo = 0;
            MiUsuarioLocal.TipoUsuario.Tipo = string.Empty;
            MiUsuarioLocal.Eliminado = false;

        }
        private void Dgv_Listado_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (Dgv_Listado.Rows.Count > 0 && Dgv_Listado.SelectedRows.Count == 1)
            {
                //Con esto partimos de un objeto limpio para evitar posibles corrupción de Información
                LimpiarUsuarioLocal();

                //Alimento los atributos del objeto con los valores de la fila que se selecciono
                MiUsuarioLocal.Id_usuario = Convert.ToInt32( Dgv_Listado.SelectedRows[0].Cells["C_Id_usuario"].Value);
                MiUsuarioLocal.Nombre = Dgv_Listado.SelectedRows[0].Cells["c_Nombre"].Value.ToString();

                MiUsuarioLocal.Cedula = Dgv_Listado.SelectedRows[0].Cells["C_Cedula"].Value.ToString();
                MiUsuarioLocal.Email = Dgv_Listado.SelectedRows[0].Cells["C_Email"].Value.ToString();
                MiUsuarioLocal.UsuarioNombre = Dgv_Listado.SelectedRows[0].Cells["C_UsuarioNombre"].Value.ToString();

                MiUsuarioLocal.TipoUsuario.Id_usuario_tipo = Convert.ToInt32(Dgv_Listado.SelectedRows[0].Cells["C_Id_usuarioTipo"].Value);
                MiUsuarioLocal.TipoUsuario.Tipo = Dgv_Listado.SelectedRows[0].Cells["C_TipoUsuario"].Value.ToString();

                //Uso los datos contenidos en los atributos para "pintarlos" en el form
                LimpiarFormulario();

                Txt_Id_Usuario.Text = MiUsuarioLocal.Id_usuario.ToString();
                Txt_Nombre.Text = MiUsuarioLocal.Nombre;
                Txt_cedula.Text = MiUsuarioLocal.Cedula;
                Txt_Email.Text = MiUsuarioLocal.Email;
                Txt_NombreUsuario.Text = MiUsuarioLocal.UsuarioNombre;
                Cbox_TipoUsuario.SelectedValue = MiUsuarioLocal.TipoUsuario.Id_usuario_tipo;

                ActivarModificarYEliminar();
                btn_modificar.Enabled = false;
                Flag_EstoyEditando = true;

                if (Cbox_VerEliminados.Checked)
                {
                    Btn_Eliminar.Text = "Activar";
                   // Btn_Eliminar.BackColor = 
                }
                else
                {
                    Btn_Eliminar.Text = "Eliminar";
                }

            }
        }

        private void ActivarAgregar()
        {
            btn_Agregar.Enabled = true;
            Btn_Eliminar.Enabled = true;
            btn_modificar.Enabled = true;
        }
        private void ActivarModificarYEliminar()
        {
            btn_Agregar.Enabled = false;
            Btn_Eliminar.Enabled = true;
            btn_modificar.Enabled = true;
        }

        private void Txt_Nombre_TextChanged(object sender, EventArgs e)
        {
            if (Flag_EstoyEditando)
            {
                btn_modificar.Enabled = true;
            }
        }

        private void Txt_cedula_TextChanged(object sender, EventArgs e)
        {
            if (Flag_EstoyEditando)
            {
                btn_modificar.Enabled = true;
            }
        }

        private void Txt_Email_TextChanged(object sender, EventArgs e)
        {
            if (Flag_EstoyEditando)
            {
                btn_modificar.Enabled = true;
            }
        }

        private void Txt_NombreUsuario_TextChanged(object sender, EventArgs e)
        {
            if (Flag_EstoyEditando)
            {
                btn_modificar.Enabled = true;
            }
        }

        private void Txt_Contrasenna_TextChanged(object sender, EventArgs e)
        {
            if (Flag_EstoyEditando)
            {
                btn_modificar.Enabled = true;
            }
        }

        private void Cbox_TipoUsuario_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Flag_EstoyEditando)
            {
                btn_modificar.Enabled = true;
            }
        }

        private void btn_modificar_Click(object sender, EventArgs e)
        {
            //Los pasos a seguir para la modificación son:
            //Una vez seleccionado una fila del datagrid y realizado un cambio en los datos
            //se debe validar que esten los datos minimos requeridos
            //la secuencia de modificación consulta primero por ID y si la respuesta es true
            //se procede con Modificar()

            if (ValidarDatosFormualrio(false))
            {
                //LLenar los datos de atributos  del objeto local de usuario
                MiUsuarioLocal = new Logica.Usuario();

                MiUsuarioLocal.Id_usuario = Convert.ToInt32(Txt_Id_Usuario.Text.Trim());
                MiUsuarioLocal.Nombre = Txt_Nombre.Text.Trim();
                MiUsuarioLocal.Cedula = Txt_cedula.Text.Trim();
                MiUsuarioLocal.Email = Txt_Email.Text.Trim();
                MiUsuarioLocal.UsuarioNombre = Txt_NombreUsuario.Text.Trim();

                MiUsuarioLocal.Contra = Txt_Contrasenna.Text.Trim();
                MiUsuarioLocal.TipoUsuario.Id_usuario_tipo = Convert.ToInt32(Cbox_TipoUsuario.SelectedValue);

                //Una vez llenos los atributos se procede a consultar contra la bd el id de el usuario
                //Esta consulta es importante por razones de concurrencia (varios usuarios afectando el mismo registro al mismo tiempo)

                if (MiUsuarioLocal.ConsultarPorID())
                {
                    //si existe se modifica
                    if (MiUsuarioLocal.Modificar())
                    {
                        //Se modifico correctamente
                        MessageBox.Show("Usuario Modificado Correctamente", ":)", MessageBoxButtons.OK);

                        LlenarListaUsuarios(true);
                        LimpiarFormulario();
                    }
                }
                
            } 
            
            }

        private void Btn_Eliminar_Click(object sender, EventArgs e)
        {
            //Aca solo hay que validar que se haya seleccionado una fila en el datagrid
            //el hecho que el cambio de texto que muestra el ID tenga datos, quiere decir que
            //cumple esta validadcion
            if (Txt_Id_Usuario.Text.Trim() != string.Empty)
            {
                //para eliminar se consulta por ID y si la respuesta es positiva se procede con Eliminar().
                //Recordar que estas eliminaciones con Logica, lo que se quiere decir que se hara una 
                //modificacion en el campo de Eliminado en la tabla.
                MiUsuarioLocal = new Logica.Usuario();

                MiUsuarioLocal.Id_usuario = Convert.ToInt32(Txt_Id_Usuario.Text.Trim());

                if (MiUsuarioLocal.ConsultarPorID())
                {
                    bool eliminar;
                    string MensajeExito;
                    if (Cbox_VerEliminados.Checked)
                    {
                        eliminar = false;
                        MensajeExito = "Usuario Activado!!!";
                    }
                    else
                    {
                        eliminar = true;
                        MensajeExito = "Usuario Eliminado!!!";
                    }
                    //Si Existe se procede con Eliminar
                    if (MiUsuarioLocal.EliminarActivar(eliminar))
                    {
                        MessageBox.Show(MensajeExito, "-.-", MessageBoxButtons.OK);

                        LlenarListaUsuarios(true);
                        LimpiarFormulario();

                        Cbox_VerEliminados.Checked = false;
                    }
                }
            }
        }

        private void Cbox_VerEliminados_Click(object sender, EventArgs e)
        {
            if (Cbox_VerEliminados.Checked)
            {
                //Indica que tiene la marca en el check y
                //queremos ver los eliminados

                LlenarListaUsuarios(false);
            }
            else
            {
                LlenarListaUsuarios(true);
            }
        }

        private void Tmr_Buscar_Tick(object sender, EventArgs e)
        {
            Tmr_Buscar.Enabled = false;
            //Aca Escribimos el Codigo para la busqueda
            if (Txt_Buscar.Text.Trim() != string.Empty)
            {
                string ValorPorBuscar = Txt_Buscar.Text.Trim();

                DataTable ListaFiltrada = new DataTable();
                ListaFiltrada = MiUsuarioLocal.ListarConFiltro(ValorPorBuscar, Cbox_Eliminado.Checked);

                if (ListaFiltrada != null && ListaFiltrada.Rows.Count > 0)
                {
                    Dgv_Listado.DataSource = ListaFiltrada;
                }
                else
                {
                    LlenarListaUsuarios(!Cbox_Eliminado.Checked);
                }

            }
        }

        private void Txt_Buscar_KeyDown(object sender, KeyEventArgs e)
        {
            Tmr_Buscar.Enabled = false;
        }

        private void Txt_Buscar_KeyUp(object sender, KeyEventArgs e)
        {
            Tmr_Buscar.Enabled = true;
        }
    }
    }
