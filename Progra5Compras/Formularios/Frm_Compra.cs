using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Progra5Compras.Formularios
{
    public partial class Frm_Compra : Form
    {
        public Logica.Compra MiCompraLocal = new Logica.Compra();
        public DataTable ListadoDetalles = new DataTable();

        

        private void EstablecerFuenteDatosDataGrid()
        {
            BindingSource MiOrigenDatos = new BindingSource();
            MiOrigenDatos.DataSource = MiCompraLocal.ListaDetalle;
            Dgr_ListaProducto.DataSource = MiOrigenDatos;

        }

        private void LimpiarForm()
        {
            MiCompraLocal = new Logica.Compra();

            MiCompraLocal.MiUsuario = Globales.Cls_Globales.MiUsuarioGlobal;

            ListadoDetalles = new DataTable();

            txt_proveedor_nombre.Text = "";
            dtp_fecha.Value = DateTime.Now.Date;

            cbox_TipoCompra.SelectedIndex = -1;

            txt_subTotal.Text = "0";
            txt_descuento.Text = "0";
            txt_impuestos.Text = "0";
            txt_total.Text = "0";
            


            EstablecerFuenteDatosDataGrid();


        }

        public Frm_Compra()
        {
            InitializeComponent();
        }

        private void btn_cerrar_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void Frm_Compra_Load(object sender, EventArgs e)
        {
            LLenarComboTipoCompra();
            LimpiarForm();
        }

        private void btn_proveedor_buscar_Click(object sender, EventArgs e)
        {
            Form MiFormDeBusqued = new Formularios.Frm_Proveedores_Buscar();

            DialogResult Resp = MiFormDeBusqued.ShowDialog();

            if (Resp == DialogResult.OK)
            {
                txt_proveedor_nombre.Text = MiCompraLocal.MiProveedor.Nombre;
            }
            else
            {
                txt_proveedor_nombre.Clear();
            }
        }

        private void LLenarComboTipoCompra()
        {

            cbox_TipoCompra.ValueMember = "id";
            cbox_TipoCompra.DisplayMember = "d";

            cbox_TipoCompra.DataSource = MiCompraLocal.MiTipoCompra.Listar();
            cbox_TipoCompra.SelectedIndex = -1;

        }

        private void btn_agregar_linea_Click(object sender, EventArgs e)
        {
            Form MiFormBusquedaProducto = new Formularios.Frm_Producto_Buscar();
            DialogResult RespuestaForm = MiFormBusquedaProducto.ShowDialog();

            if (RespuestaForm == DialogResult.OK)
            {
                EstablecerFuenteDatosDataGrid();
            }
        }

        private bool Totalizar()
        {
            bool R = false;
            try
            {
                decimal Subt, Descuentos, Impuestos, Total;

                Subt = 0;
                Descuentos = 0;
                Impuestos = 0;
                Total = 0;

                if (MiCompraLocal != null && MiCompraLocal.ListaDetalle.Count > 0)
                {
                    foreach (Logica.Compra_Detalle item in MiCompraLocal.ListaDetalle)
                    {
                        decimal Precio = item.PrecioUnitario;
                        decimal Cantidad = item.Cantidad;
                        decimal PorcentajeDesc = item.PorcentajeDescuento;

                        decimal TasaImpuesto = item.TasaImpuesto();

                        //Se calculo cada uno de los rubros principales de la LINEA

                        decimal subt = Precio * Cantidad;
                        decimal desclinea = (subt * PorcentajeDesc) / 100;

                        decimal subt2 = subt - desclinea;

                        decimal impuestolinea = (subt2 * TasaImpuesto) / 100;
                        decimal totallinea = subt2 + impuestolinea;

                        //Se asignan los valores que hacian falta a cada linea.
                        item.Descuentos = desclinea;
                        item.Impuesto = impuestolinea;
                        item.TotalLinea = totallinea;

                        //Se suman los montos correspondientes a las variables de totalizacion general

                        Subt += subt; // Es equivalente a Subt = Subt+ subt

                        Descuentos += desclinea;
                        Impuestos += impuestolinea;
                        Total += totallinea;
                    }
                }
                //Se dibujan los valores en los cuadros de texto correspondientes
                txt_subTotal.Text = Subt.ToString();
                txt_descuento.Text = Descuentos.ToString();
                txt_impuestos.Text = Impuestos.ToString();
                txt_total.Text = Total.ToString();
            }
            catch (Exception Ex)
            {
                MessageBox.Show("Algo salio mal en el totalizador: " + Ex.Message.ToString(), ":(", MessageBoxButtons.OK);
            }

            return R;

        }

        private void Dgr_ListaProducto_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            Dgr_ListaProducto.ClearSelection();
            Totalizar();
        }

        private void btn_procesar_Click(object sender, EventArgs e)
        {
            //Primero se valida que esten los datos requeridos 
            if (ValidarCompra())
            {
                //Para los objetos compuestos de encabezado y detalle 
                //primero se hace un insert en la tabla encabezado,
                //se captura el ID recien ingresado ya que lo necesitaremos en el
                //FK de la tabla de detalle (el FK a producto ya lo tenemos)
                //por ultimo se hace un recorrido de las lineas del detalle del
                //objeto compuesto y se hace un insert en la tabla de detalle por
                //cada iteración

                MiCompraLocal.Fecha = dtp_fecha.Value.Date;
                MiCompraLocal.MiTipoCompra.ID_Tipo_Compra = Convert.ToInt32(cbox_TipoCompra.SelectedValue);
                int idFactura = MiCompraLocal.Agregar();
                if (idFactura > 0)
                {
                    //La compra se registro correctamente
                    //se procede a generar el reporte y mostrarlo en pantalla

                    MessageBox.Show("Compra Procesada Correctamente", ":)", MessageBoxButtons.OK);

                }


            }

        }

        private bool ValidarCompra()
        {
            bool R = false;

            //que tenga proveedor, y que hatan detalles
            if (MiCompraLocal.MiProveedor.Id_Proveedor > 0 &&
                MiCompraLocal.ListaDetalle.Count > 0 &&
                cbox_TipoCompra.SelectedIndex >= 0)
            {
                R = true;
            }

            return R;
        }
    }
}
