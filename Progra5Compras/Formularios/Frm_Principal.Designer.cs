namespace Progra5Compras.Formularios
{
    partial class Frm_Principal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.MNUMantenimientos = new System.Windows.Forms.ToolStripMenuItem();
            this.MNUUsuarios = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.MNUEmpresa = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.MNUProveedores = new System.Windows.Forms.ToolStripMenuItem();
            this.MNUProductos = new System.Windows.Forms.ToolStripMenuItem();
            this.MNUProcesos = new System.Windows.Forms.ToolStripMenuItem();
            this.MNURegistroCompras = new System.Windows.Forms.ToolStripMenuItem();
            this.MNUReportes = new System.Windows.Forms.ToolStripMenuItem();
            this.MNUAcercaDe = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MNUMantenimientos,
            this.MNUProcesos,
            this.MNUReportes,
            this.MNUAcercaDe});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1423, 28);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // MNUMantenimientos
            // 
            this.MNUMantenimientos.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MNUUsuarios,
            this.toolStripSeparator1,
            this.MNUEmpresa,
            this.toolStripSeparator2,
            this.MNUProveedores,
            this.MNUProductos});
            this.MNUMantenimientos.Name = "MNUMantenimientos";
            this.MNUMantenimientos.Size = new System.Drawing.Size(130, 24);
            this.MNUMantenimientos.Text = "Mantenimientos";
            // 
            // MNUUsuarios
            // 
            this.MNUUsuarios.Name = "MNUUsuarios";
            this.MNUUsuarios.Size = new System.Drawing.Size(174, 26);
            this.MNUUsuarios.Text = "Usuarios";
            this.MNUUsuarios.Click += new System.EventHandler(this.MNUUsuarios_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(171, 6);
            // 
            // MNUEmpresa
            // 
            this.MNUEmpresa.Name = "MNUEmpresa";
            this.MNUEmpresa.Size = new System.Drawing.Size(174, 26);
            this.MNUEmpresa.Text = "Empresa";
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(171, 6);
            // 
            // MNUProveedores
            // 
            this.MNUProveedores.Name = "MNUProveedores";
            this.MNUProveedores.Size = new System.Drawing.Size(174, 26);
            this.MNUProveedores.Text = "Proveedores";
            // 
            // MNUProductos
            // 
            this.MNUProductos.Name = "MNUProductos";
            this.MNUProductos.Size = new System.Drawing.Size(174, 26);
            this.MNUProductos.Text = "Productos";
            // 
            // MNUProcesos
            // 
            this.MNUProcesos.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MNURegistroCompras});
            this.MNUProcesos.Name = "MNUProcesos";
            this.MNUProcesos.Size = new System.Drawing.Size(81, 24);
            this.MNUProcesos.Text = "Procesos";
            // 
            // MNURegistroCompras
            // 
            this.MNURegistroCompras.Name = "MNURegistroCompras";
            this.MNURegistroCompras.Size = new System.Drawing.Size(231, 26);
            this.MNURegistroCompras.Text = "Registro de Compras";
            this.MNURegistroCompras.Click += new System.EventHandler(this.MNURegistroCompras_Click);
            // 
            // MNUReportes
            // 
            this.MNUReportes.Name = "MNUReportes";
            this.MNUReportes.Size = new System.Drawing.Size(82, 24);
            this.MNUReportes.Text = "Reportes";
            // 
            // MNUAcercaDe
            // 
            this.MNUAcercaDe.Name = "MNUAcercaDe";
            this.MNUAcercaDe.Size = new System.Drawing.Size(98, 24);
            this.MNUAcercaDe.Text = "Acerca de...";
            // 
            // Frm_Principal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1423, 820);
            this.Controls.Add(this.menuStrip1);
            this.DoubleBuffered = true;
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "Frm_Principal";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Sistema de Control de Compras";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Frm_Principal_FormClosed);
            this.Load += new System.EventHandler(this.Frm_Principal_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem MNUMantenimientos;
        private System.Windows.Forms.ToolStripMenuItem MNUProcesos;
        private System.Windows.Forms.ToolStripMenuItem MNUReportes;
        private System.Windows.Forms.ToolStripMenuItem MNUAcercaDe;
        private System.Windows.Forms.ToolStripMenuItem MNUUsuarios;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem MNUEmpresa;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem MNUProveedores;
        private System.Windows.Forms.ToolStripMenuItem MNUProductos;
        private System.Windows.Forms.ToolStripMenuItem MNURegistroCompras;
    }
}