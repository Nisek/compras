namespace Progra5Compras.Formularios
{
    partial class Frm_Producto_Buscar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_cerrar = new System.Windows.Forms.Button();
            this.btn_selecionar = new System.Windows.Forms.Button();
            this.dgv_lista = new System.Windows.Forms.DataGridView();
            this.C_Id_producto = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.C_CodigoBarras = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.C_Nombre = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.C_Cantidad = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.C_PrecioUnitario = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.C_Descripcion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.C_Eliminado = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txt_buscar = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txt_precio = new System.Windows.Forms.TextBox();
            this.txt_cantidad = new System.Windows.Forms.NumericUpDown();
            this.txt_23 = new System.Windows.Forms.Label();
            this.txt_descuento = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_lista)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txt_cantidad)).BeginInit();
            this.SuspendLayout();
            // 
            // btn_cerrar
            // 
            this.btn_cerrar.BackColor = System.Drawing.Color.DarkRed;
            this.btn_cerrar.ForeColor = System.Drawing.Color.White;
            this.btn_cerrar.Location = new System.Drawing.Point(468, 372);
            this.btn_cerrar.Name = "btn_cerrar";
            this.btn_cerrar.Size = new System.Drawing.Size(135, 47);
            this.btn_cerrar.TabIndex = 7;
            this.btn_cerrar.Text = "Cerrar";
            this.btn_cerrar.UseVisualStyleBackColor = false;
            this.btn_cerrar.Click += new System.EventHandler(this.btn_cerrar_Click);
            // 
            // btn_selecionar
            // 
            this.btn_selecionar.BackColor = System.Drawing.Color.Green;
            this.btn_selecionar.ForeColor = System.Drawing.Color.White;
            this.btn_selecionar.Location = new System.Drawing.Point(150, 372);
            this.btn_selecionar.Name = "btn_selecionar";
            this.btn_selecionar.Size = new System.Drawing.Size(215, 47);
            this.btn_selecionar.TabIndex = 6;
            this.btn_selecionar.Text = "Selecionar";
            this.btn_selecionar.UseVisualStyleBackColor = false;
            this.btn_selecionar.Click += new System.EventHandler(this.btn_selecionar_Click);
            // 
            // dgv_lista
            // 
            this.dgv_lista.AllowUserToAddRows = false;
            this.dgv_lista.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_lista.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.C_Id_producto,
            this.C_CodigoBarras,
            this.C_Nombre,
            this.C_Cantidad,
            this.C_PrecioUnitario,
            this.C_Descripcion,
            this.C_Eliminado});
            this.dgv_lista.Location = new System.Drawing.Point(12, 42);
            this.dgv_lista.Name = "dgv_lista";
            this.dgv_lista.RowHeadersVisible = false;
            this.dgv_lista.RowHeadersWidth = 51;
            this.dgv_lista.RowTemplate.Height = 24;
            this.dgv_lista.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv_lista.Size = new System.Drawing.Size(763, 193);
            this.dgv_lista.TabIndex = 5;
            this.dgv_lista.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_lista_CellClick);
            this.dgv_lista.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.dgv_lista_DataBindingComplete);
            // 
            // C_Id_producto
            // 
            this.C_Id_producto.DataPropertyName = "Id_producto";
            this.C_Id_producto.HeaderText = "Código";
            this.C_Id_producto.MinimumWidth = 6;
            this.C_Id_producto.Name = "C_Id_producto";
            this.C_Id_producto.Width = 125;
            // 
            // C_CodigoBarras
            // 
            this.C_CodigoBarras.DataPropertyName = "CodigoBarras";
            this.C_CodigoBarras.HeaderText = "C. Barras";
            this.C_CodigoBarras.MinimumWidth = 6;
            this.C_CodigoBarras.Name = "C_CodigoBarras";
            this.C_CodigoBarras.Width = 125;
            // 
            // C_Nombre
            // 
            this.C_Nombre.DataPropertyName = "Nombre";
            this.C_Nombre.HeaderText = "Producto";
            this.C_Nombre.MinimumWidth = 6;
            this.C_Nombre.Name = "C_Nombre";
            this.C_Nombre.Width = 125;
            // 
            // C_Cantidad
            // 
            this.C_Cantidad.DataPropertyName = "Cantidad";
            this.C_Cantidad.HeaderText = "Stock";
            this.C_Cantidad.MinimumWidth = 6;
            this.C_Cantidad.Name = "C_Cantidad";
            this.C_Cantidad.Width = 125;
            // 
            // C_PrecioUnitario
            // 
            this.C_PrecioUnitario.DataPropertyName = "PrecioUnitario";
            this.C_PrecioUnitario.HeaderText = "Precio";
            this.C_PrecioUnitario.MinimumWidth = 6;
            this.C_PrecioUnitario.Name = "C_PrecioUnitario";
            this.C_PrecioUnitario.Width = 125;
            // 
            // C_Descripcion
            // 
            this.C_Descripcion.DataPropertyName = "Descripcion";
            this.C_Descripcion.HeaderText = "Descripción";
            this.C_Descripcion.MinimumWidth = 6;
            this.C_Descripcion.Name = "C_Descripcion";
            this.C_Descripcion.Width = 125;
            // 
            // C_Eliminado
            // 
            this.C_Eliminado.DataPropertyName = "Eliminado";
            this.C_Eliminado.HeaderText = "Eliminado";
            this.C_Eliminado.MinimumWidth = 6;
            this.C_Eliminado.Name = "C_Eliminado";
            this.C_Eliminado.Visible = false;
            this.C_Eliminado.Width = 125;
            // 
            // txt_buscar
            // 
            this.txt_buscar.Location = new System.Drawing.Point(157, 14);
            this.txt_buscar.Name = "txt_buscar";
            this.txt_buscar.Size = new System.Drawing.Size(446, 22);
            this.txt_buscar.TabIndex = 4;
            this.txt_buscar.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txt_precio);
            this.groupBox1.Controls.Add(this.txt_cantidad);
            this.groupBox1.Controls.Add(this.txt_23);
            this.groupBox1.Controls.Add(this.txt_descuento);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(13, 260);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(762, 100);
            this.groupBox1.TabIndex = 8;
            this.groupBox1.TabStop = false;
            // 
            // txt_precio
            // 
            this.txt_precio.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_precio.Location = new System.Drawing.Point(542, 52);
            this.txt_precio.Name = "txt_precio";
            this.txt_precio.Size = new System.Drawing.Size(134, 30);
            this.txt_precio.TabIndex = 5;
            this.txt_precio.Text = "0";
            this.txt_precio.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txt_precio.TextChanged += new System.EventHandler(this.textBox2_TextChanged);
            // 
            // txt_cantidad
            // 
            this.txt_cantidad.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_cantidad.Location = new System.Drawing.Point(75, 53);
            this.txt_cantidad.Name = "txt_cantidad";
            this.txt_cantidad.Size = new System.Drawing.Size(120, 30);
            this.txt_cantidad.TabIndex = 3;
            this.txt_cantidad.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txt_cantidad.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // txt_23
            // 
            this.txt_23.AutoSize = true;
            this.txt_23.Location = new System.Drawing.Point(560, 22);
            this.txt_23.Name = "txt_23";
            this.txt_23.Size = new System.Drawing.Size(101, 17);
            this.txt_23.TabIndex = 4;
            this.txt_23.Text = "Precio Compra";
            // 
            // txt_descuento
            // 
            this.txt_descuento.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_descuento.Location = new System.Drawing.Point(324, 53);
            this.txt_descuento.Name = "txt_descuento";
            this.txt_descuento.Size = new System.Drawing.Size(130, 30);
            this.txt_descuento.TabIndex = 2;
            this.txt_descuento.Text = "0";
            this.txt_descuento.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txt_descuento.TextChanged += new System.EventHandler(this.txt_descuento_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(354, 22);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(76, 17);
            this.label2.TabIndex = 1;
            this.label2.Text = "Descuento";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(104, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(64, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Cantidad";
            // 
            // Frm_Producto_Buscar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(788, 431);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btn_cerrar);
            this.Controls.Add(this.btn_selecionar);
            this.Controls.Add(this.dgv_lista);
            this.Controls.Add(this.txt_buscar);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "Frm_Producto_Buscar";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Busqueda de Productos";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.Frm_Producto_Buscar_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_lista)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txt_cantidad)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_cerrar;
        private System.Windows.Forms.Button btn_selecionar;
        private System.Windows.Forms.DataGridView dgv_lista;
        private System.Windows.Forms.TextBox txt_buscar;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txt_descuento;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown txt_cantidad;
        private System.Windows.Forms.TextBox txt_precio;
        private System.Windows.Forms.Label txt_23;
        private System.Windows.Forms.DataGridViewTextBoxColumn C_Id_producto;
        private System.Windows.Forms.DataGridViewTextBoxColumn C_CodigoBarras;
        private System.Windows.Forms.DataGridViewTextBoxColumn C_Nombre;
        private System.Windows.Forms.DataGridViewTextBoxColumn C_Cantidad;
        private System.Windows.Forms.DataGridViewTextBoxColumn C_PrecioUnitario;
        private System.Windows.Forms.DataGridViewTextBoxColumn C_Descripcion;
        private System.Windows.Forms.DataGridViewTextBoxColumn C_Eliminado;
    }
}