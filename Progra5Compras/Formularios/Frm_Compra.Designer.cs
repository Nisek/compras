namespace Progra5Compras.Formularios
{
    partial class Frm_Compra
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_Compra));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lb_usuario_nombre = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.cbox_TipoCompra = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.dtp_fecha = new System.Windows.Forms.DateTimePicker();
            this.btn_proveedor_buscar = new System.Windows.Forms.Button();
            this.txt_proveedor_nombre = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.btn_agregar_linea = new System.Windows.Forms.ToolStripButton();
            this.btn_modificar_linea = new System.Windows.Forms.ToolStripButton();
            this.btn_quitar_linea = new System.Windows.Forms.ToolStripButton();
            this.Dgr_ListaProducto = new System.Windows.Forms.DataGridView();
            this.C_Id_producto = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.C_NombreProducto = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.C_Cantidad = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.C_PrecioUnitario = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.C_PorcentajeDescuento = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.C_Descuento = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.C_Impuestos = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.C_TotalLinea = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.txt_total = new System.Windows.Forms.TextBox();
            this.lb123 = new System.Windows.Forms.Label();
            this.txt_impuestos = new System.Windows.Forms.TextBox();
            this.lb_rt56 = new System.Windows.Forms.Label();
            this.txt_descuento = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txt_subTotal = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.btn_procesar = new System.Windows.Forms.Button();
            this.btn_cerrar = new System.Windows.Forms.Button();
            this.menuStrip2 = new System.Windows.Forms.MenuStrip();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Dgr_ListaProducto)).BeginInit();
            this.groupBox3.SuspendLayout();
            this.menuStrip2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lb_usuario_nombre);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.cbox_TipoCompra);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.dtp_fecha);
            this.groupBox1.Controls.Add(this.btn_proveedor_buscar);
            this.groupBox1.Controls.Add(this.txt_proveedor_nombre);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(2, 44);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(991, 132);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Encabesado";
            // 
            // lb_usuario_nombre
            // 
            this.lb_usuario_nombre.AutoSize = true;
            this.lb_usuario_nombre.Location = new System.Drawing.Point(164, 76);
            this.lb_usuario_nombre.Name = "lb_usuario_nombre";
            this.lb_usuario_nombre.Size = new System.Drawing.Size(130, 17);
            this.lb_usuario_nombre.TabIndex = 8;
            this.lb_usuario_nombre.Text = "lb_usuario_nombre";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(10, 76);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(114, 17);
            this.label4.TabIndex = 7;
            this.label4.Text = "Usuario Registra";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(607, 72);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(89, 17);
            this.label3.TabIndex = 6;
            this.label3.Text = "Tipo Compra";
            // 
            // cbox_TipoCompra
            // 
            this.cbox_TipoCompra.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbox_TipoCompra.FormattingEnabled = true;
            this.cbox_TipoCompra.Location = new System.Drawing.Point(721, 69);
            this.cbox_TipoCompra.Name = "cbox_TipoCompra";
            this.cbox_TipoCompra.Size = new System.Drawing.Size(246, 24);
            this.cbox_TipoCompra.TabIndex = 5;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(649, 23);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(47, 17);
            this.label2.TabIndex = 4;
            this.label2.Text = "Fecha";
            // 
            // dtp_fecha
            // 
            this.dtp_fecha.Location = new System.Drawing.Point(721, 21);
            this.dtp_fecha.Name = "dtp_fecha";
            this.dtp_fecha.Size = new System.Drawing.Size(246, 22);
            this.dtp_fecha.TabIndex = 3;
            // 
            // btn_proveedor_buscar
            // 
            this.btn_proveedor_buscar.Location = new System.Drawing.Point(483, 20);
            this.btn_proveedor_buscar.Name = "btn_proveedor_buscar";
            this.btn_proveedor_buscar.Size = new System.Drawing.Size(75, 23);
            this.btn_proveedor_buscar.TabIndex = 2;
            this.btn_proveedor_buscar.Text = "Buscar";
            this.btn_proveedor_buscar.UseVisualStyleBackColor = true;
            this.btn_proveedor_buscar.Click += new System.EventHandler(this.btn_proveedor_buscar_Click);
            // 
            // txt_proveedor_nombre
            // 
            this.txt_proveedor_nombre.Location = new System.Drawing.Point(97, 22);
            this.txt_proveedor_nombre.Name = "txt_proveedor_nombre";
            this.txt_proveedor_nombre.Size = new System.Drawing.Size(365, 22);
            this.txt_proveedor_nombre.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(10, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(74, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Proveedor";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.toolStrip1);
            this.groupBox2.Controls.Add(this.Dgr_ListaProducto);
            this.groupBox2.Location = new System.Drawing.Point(2, 182);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(991, 246);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Detalle";
            // 
            // toolStrip1
            // 
            this.toolStrip1.Dock = System.Windows.Forms.DockStyle.Right;
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btn_agregar_linea,
            this.btn_modificar_linea,
            this.btn_quitar_linea});
            this.toolStrip1.Location = new System.Drawing.Point(890, 18);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(98, 225);
            this.toolStrip1.TabIndex = 1;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // btn_agregar_linea
            // 
            this.btn_agregar_linea.Image = ((System.Drawing.Image)(resources.GetObject("btn_agregar_linea.Image")));
            this.btn_agregar_linea.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_agregar_linea.Name = "btn_agregar_linea";
            this.btn_agregar_linea.Size = new System.Drawing.Size(95, 24);
            this.btn_agregar_linea.Text = "Agregar";
            this.btn_agregar_linea.Click += new System.EventHandler(this.btn_agregar_linea_Click);
            // 
            // btn_modificar_linea
            // 
            this.btn_modificar_linea.Image = ((System.Drawing.Image)(resources.GetObject("btn_modificar_linea.Image")));
            this.btn_modificar_linea.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_modificar_linea.Name = "btn_modificar_linea";
            this.btn_modificar_linea.Size = new System.Drawing.Size(95, 24);
            this.btn_modificar_linea.Text = "Modificar";
            // 
            // btn_quitar_linea
            // 
            this.btn_quitar_linea.Image = ((System.Drawing.Image)(resources.GetObject("btn_quitar_linea.Image")));
            this.btn_quitar_linea.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_quitar_linea.Name = "btn_quitar_linea";
            this.btn_quitar_linea.Size = new System.Drawing.Size(95, 24);
            this.btn_quitar_linea.Text = "Quitar";
            // 
            // Dgr_ListaProducto
            // 
            this.Dgr_ListaProducto.AllowUserToAddRows = false;
            this.Dgr_ListaProducto.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.Dgr_ListaProducto.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.C_Id_producto,
            this.C_NombreProducto,
            this.C_Cantidad,
            this.C_PrecioUnitario,
            this.C_PorcentajeDescuento,
            this.C_Descuento,
            this.C_Impuestos,
            this.C_TotalLinea});
            this.Dgr_ListaProducto.Location = new System.Drawing.Point(6, 21);
            this.Dgr_ListaProducto.MultiSelect = false;
            this.Dgr_ListaProducto.Name = "Dgr_ListaProducto";
            this.Dgr_ListaProducto.RowHeadersVisible = false;
            this.Dgr_ListaProducto.RowHeadersWidth = 51;
            this.Dgr_ListaProducto.RowTemplate.Height = 24;
            this.Dgr_ListaProducto.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.Dgr_ListaProducto.Size = new System.Drawing.Size(881, 219);
            this.Dgr_ListaProducto.TabIndex = 0;
            this.Dgr_ListaProducto.VirtualMode = true;
            this.Dgr_ListaProducto.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.Dgr_ListaProducto_DataBindingComplete);
            // 
            // C_Id_producto
            // 
            this.C_Id_producto.DataPropertyName = "Id_producto";
            this.C_Id_producto.HeaderText = "Código";
            this.C_Id_producto.MinimumWidth = 6;
            this.C_Id_producto.Name = "C_Id_producto";
            this.C_Id_producto.Width = 125;
            // 
            // C_NombreProducto
            // 
            this.C_NombreProducto.DataPropertyName = "NombreProducto";
            this.C_NombreProducto.HeaderText = "Producto";
            this.C_NombreProducto.MinimumWidth = 6;
            this.C_NombreProducto.Name = "C_NombreProducto";
            this.C_NombreProducto.Width = 125;
            // 
            // C_Cantidad
            // 
            this.C_Cantidad.DataPropertyName = "Cantidad";
            this.C_Cantidad.HeaderText = "Cantidad";
            this.C_Cantidad.MinimumWidth = 6;
            this.C_Cantidad.Name = "C_Cantidad";
            this.C_Cantidad.Width = 125;
            // 
            // C_PrecioUnitario
            // 
            this.C_PrecioUnitario.DataPropertyName = "PrecioUnitario";
            this.C_PrecioUnitario.HeaderText = "Precio";
            this.C_PrecioUnitario.MinimumWidth = 6;
            this.C_PrecioUnitario.Name = "C_PrecioUnitario";
            this.C_PrecioUnitario.Width = 125;
            // 
            // C_PorcentajeDescuento
            // 
            this.C_PorcentajeDescuento.DataPropertyName = "PorcentajeDescuento";
            this.C_PorcentajeDescuento.HeaderText = "%Desc.";
            this.C_PorcentajeDescuento.MinimumWidth = 6;
            this.C_PorcentajeDescuento.Name = "C_PorcentajeDescuento";
            this.C_PorcentajeDescuento.Width = 125;
            // 
            // C_Descuento
            // 
            this.C_Descuento.DataPropertyName = "Descuentos";
            this.C_Descuento.HeaderText = "Total Descuento";
            this.C_Descuento.MinimumWidth = 6;
            this.C_Descuento.Name = "C_Descuento";
            this.C_Descuento.Width = 125;
            // 
            // C_Impuestos
            // 
            this.C_Impuestos.DataPropertyName = "Impuestos";
            this.C_Impuestos.HeaderText = "Impuestos";
            this.C_Impuestos.MinimumWidth = 6;
            this.C_Impuestos.Name = "C_Impuestos";
            this.C_Impuestos.Width = 125;
            // 
            // C_TotalLinea
            // 
            this.C_TotalLinea.DataPropertyName = "TotalLinea";
            this.C_TotalLinea.HeaderText = "TOTAL";
            this.C_TotalLinea.MinimumWidth = 6;
            this.C_TotalLinea.Name = "C_TotalLinea";
            this.C_TotalLinea.Width = 125;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.txt_total);
            this.groupBox3.Controls.Add(this.lb123);
            this.groupBox3.Controls.Add(this.txt_impuestos);
            this.groupBox3.Controls.Add(this.lb_rt56);
            this.groupBox3.Controls.Add(this.txt_descuento);
            this.groupBox3.Controls.Add(this.label6);
            this.groupBox3.Controls.Add(this.txt_subTotal);
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.Location = new System.Drawing.Point(2, 434);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(718, 123);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Totales";
            // 
            // txt_total
            // 
            this.txt_total.BackColor = System.Drawing.Color.Black;
            this.txt_total.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_total.ForeColor = System.Drawing.Color.Red;
            this.txt_total.Location = new System.Drawing.Point(483, 64);
            this.txt_total.Name = "txt_total";
            this.txt_total.ReadOnly = true;
            this.txt_total.Size = new System.Drawing.Size(223, 34);
            this.txt_total.TabIndex = 7;
            this.txt_total.Text = "0";
            this.txt_total.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lb123
            // 
            this.lb123.AutoSize = true;
            this.lb123.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb123.Location = new System.Drawing.Point(567, 34);
            this.lb123.Name = "lb123";
            this.lb123.Size = new System.Drawing.Size(51, 20);
            this.lb123.TabIndex = 6;
            this.lb123.Text = "Total";
            // 
            // txt_impuestos
            // 
            this.txt_impuestos.BackColor = System.Drawing.Color.Black;
            this.txt_impuestos.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_impuestos.ForeColor = System.Drawing.Color.Red;
            this.txt_impuestos.Location = new System.Drawing.Point(335, 64);
            this.txt_impuestos.Name = "txt_impuestos";
            this.txt_impuestos.ReadOnly = true;
            this.txt_impuestos.Size = new System.Drawing.Size(127, 34);
            this.txt_impuestos.TabIndex = 5;
            this.txt_impuestos.Text = "0";
            this.txt_impuestos.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lb_rt56
            // 
            this.lb_rt56.AutoSize = true;
            this.lb_rt56.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_rt56.Location = new System.Drawing.Point(352, 34);
            this.lb_rt56.Name = "lb_rt56";
            this.lb_rt56.Size = new System.Drawing.Size(95, 20);
            this.lb_rt56.TabIndex = 4;
            this.lb_rt56.Text = "Impuestos";
            // 
            // txt_descuento
            // 
            this.txt_descuento.BackColor = System.Drawing.Color.Black;
            this.txt_descuento.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_descuento.ForeColor = System.Drawing.Color.Red;
            this.txt_descuento.Location = new System.Drawing.Point(177, 64);
            this.txt_descuento.Name = "txt_descuento";
            this.txt_descuento.ReadOnly = true;
            this.txt_descuento.Size = new System.Drawing.Size(127, 34);
            this.txt_descuento.TabIndex = 3;
            this.txt_descuento.Text = "0";
            this.txt_descuento.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(192, 34);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(99, 20);
            this.label6.TabIndex = 2;
            this.label6.Text = "Descuento";
            // 
            // txt_subTotal
            // 
            this.txt_subTotal.BackColor = System.Drawing.Color.Black;
            this.txt_subTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_subTotal.ForeColor = System.Drawing.Color.Red;
            this.txt_subTotal.Location = new System.Drawing.Point(22, 64);
            this.txt_subTotal.Name = "txt_subTotal";
            this.txt_subTotal.ReadOnly = true;
            this.txt_subTotal.Size = new System.Drawing.Size(127, 34);
            this.txt_subTotal.TabIndex = 1;
            this.txt_subTotal.Text = "0";
            this.txt_subTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(43, 34);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(89, 20);
            this.label5.TabIndex = 0;
            this.label5.Text = "Sub Total";
            // 
            // btn_procesar
            // 
            this.btn_procesar.BackColor = System.Drawing.Color.DarkGreen;
            this.btn_procesar.ForeColor = System.Drawing.Color.White;
            this.btn_procesar.Location = new System.Drawing.Point(723, 446);
            this.btn_procesar.Name = "btn_procesar";
            this.btn_procesar.Size = new System.Drawing.Size(172, 102);
            this.btn_procesar.TabIndex = 8;
            this.btn_procesar.Text = "Procesar Compra";
            this.btn_procesar.UseVisualStyleBackColor = false;
            this.btn_procesar.Click += new System.EventHandler(this.btn_procesar_Click);
            // 
            // btn_cerrar
            // 
            this.btn_cerrar.BackColor = System.Drawing.Color.DarkRed;
            this.btn_cerrar.ForeColor = System.Drawing.Color.White;
            this.btn_cerrar.Location = new System.Drawing.Point(901, 478);
            this.btn_cerrar.Name = "btn_cerrar";
            this.btn_cerrar.Size = new System.Drawing.Size(107, 54);
            this.btn_cerrar.TabIndex = 9;
            this.btn_cerrar.Text = "Cerrar";
            this.btn_cerrar.UseVisualStyleBackColor = false;
            this.btn_cerrar.Click += new System.EventHandler(this.btn_cerrar_Click);
            // 
            // menuStrip2
            // 
            this.menuStrip2.BackColor = System.Drawing.Color.Black;
            this.menuStrip2.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1});
            this.menuStrip2.Location = new System.Drawing.Point(0, 0);
            this.menuStrip2.Name = "menuStrip2";
            this.menuStrip2.Size = new System.Drawing.Size(1008, 30);
            this.menuStrip2.TabIndex = 11;
            this.menuStrip2.Text = "Nueva Compra";
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.BackColor = System.Drawing.Color.DarkRed;
            this.toolStripMenuItem1.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolStripMenuItem1.ForeColor = System.Drawing.Color.White;
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(127, 26);
            this.toolStripMenuItem1.Text = "Nueva Compra";
            // 
            // Frm_Compra
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1008, 555);
            this.Controls.Add(this.btn_cerrar);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.btn_procesar);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.menuStrip2);
            this.DoubleBuffered = true;
            this.MaximizeBox = false;
            this.Name = "Frm_Compra";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Registro de Compra";
            this.Load += new System.EventHandler(this.Frm_Compra_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Dgr_ListaProducto)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.menuStrip2.ResumeLayout(false);
            this.menuStrip2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cbox_TipoCompra;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker dtp_fecha;
        private System.Windows.Forms.Button btn_proveedor_buscar;
        private System.Windows.Forms.TextBox txt_proveedor_nombre;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lb_usuario_nombre;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DataGridView Dgr_ListaProducto;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton btn_agregar_linea;
        private System.Windows.Forms.ToolStripButton btn_modificar_linea;
        private System.Windows.Forms.ToolStripButton btn_quitar_linea;
        private System.Windows.Forms.TextBox txt_subTotal;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txt_total;
        private System.Windows.Forms.Label lb123;
        private System.Windows.Forms.TextBox txt_impuestos;
        private System.Windows.Forms.Label lb_rt56;
        private System.Windows.Forms.TextBox txt_descuento;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btn_cerrar;
        private System.Windows.Forms.Button btn_procesar;
        private System.Windows.Forms.MenuStrip menuStrip2;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.DataGridViewTextBoxColumn C_Id_producto;
        private System.Windows.Forms.DataGridViewTextBoxColumn C_NombreProducto;
        private System.Windows.Forms.DataGridViewTextBoxColumn C_Cantidad;
        private System.Windows.Forms.DataGridViewTextBoxColumn C_PrecioUnitario;
        private System.Windows.Forms.DataGridViewTextBoxColumn C_PorcentajeDescuento;
        private System.Windows.Forms.DataGridViewTextBoxColumn C_Descuento;
        private System.Windows.Forms.DataGridViewTextBoxColumn C_Impuestos;
        private System.Windows.Forms.DataGridViewTextBoxColumn C_TotalLinea;
    }
}