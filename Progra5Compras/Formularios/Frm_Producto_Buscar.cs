using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Progra5Compras.Formularios
{
    public partial class Frm_Producto_Buscar : Form
    {
        Logica.Producto ObjProducto = new Logica.Producto();
        DataTable ListaProductos = new DataTable();


        private void LlenarListaProductos()
        {
            ListaProductos = new DataTable();
            ListaProductos = ObjProducto.Listar();
            dgv_lista.DataSource = ListaProductos;




        }

        public Frm_Producto_Buscar()
        {
            InitializeComponent();
        }

        private void Frm_Producto_Buscar_Load(object sender, EventArgs e)
        {
            LlenarListaProductos();
        }

        private void txt_descuento_TextChanged(object sender, EventArgs e)
        {
            if (txt_descuento.Text.Trim() == string.Empty)
            {
                txt_descuento.Text = "0";
                txt_descuento.SelectAll();
            }
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            txt_descuento.Text = "0";
            txt_descuento.SelectAll();
        }

        private void btn_selecionar_Click(object sender, EventArgs e)
        {
            if (dgv_lista.Rows.Count > 0 && dgv_lista.SelectedRows.Count == 1)
            {
                //Se valida que hayan datos en los detalles de cantidad, desc y precio.
                if (ValidarDatos())
                {
                    //Todo validado, se puede proceder.

                    Logica.Compra_Detalle ObjDetalle = new Logica.Compra_Detalle();

                    ObjDetalle.IdProducto = Convert.ToInt32(dgv_lista.SelectedRows[0].Cells["C_Id_producto"].Value);
                    ObjDetalle.NombreProducto = dgv_lista.SelectedRows[0].Cells["C_Nombre"].Value.ToString();
                    ObjDetalle.Cantidad = Convert.ToDecimal(txt_cantidad.Text.Trim());
                    ObjDetalle.PorcentajeDescuento = Convert.ToDecimal(txt_descuento.Text.Trim());
                    ObjDetalle.PrecioUnitario = Convert.ToDecimal(txt_precio.Text.Trim());

                    ObjDetalle.Impuesto = 0;
                    ObjDetalle.Descuentos = 0;
                    ObjDetalle.TotalLinea = 0;

                    Globales.Cls_Globales.MiFormDeCompras.MiCompraLocal.ListaDetalle.Add(ObjDetalle);

                    //Una vez que se agrega una linea de detalle se retorna un Ok al form
                    //de registro de compras

                    this.DialogResult = DialogResult.OK;
                }

            }
        }

        private bool ValidarDatos()
        {
            bool R = false;

            try
            {
                if (txt_cantidad.Value > 0 &&
                    txt_descuento.Text.Trim() != string.Empty &&
                    Convert.ToDecimal(txt_descuento.Text.Trim()) >= 0 &&
                    Convert.ToDecimal(txt_descuento.Text.Trim()) <= 100 &&
                    txt_precio.Text.Trim() != string.Empty &&
                    Convert.ToDecimal(txt_precio.Text.Trim()) >= 0)
                {
                    R = true;
                }
                else
                {
                    //Si no se valido correctamente se debe indicar que carajos fallo

                    if (txt_cantidad.Value <= 0)
                    {
                        MessageBox.Show("La cantidad no puede ser igual o menor a cero");
                        txt_cantidad.Focus();
                    }
                    if (txt_descuento.Text.Trim() == string.Empty || 
                        Convert.ToDecimal(txt_descuento.Text.Trim()) < 0 ||
                        Convert.ToDecimal(txt_descuento.Text.Trim()) > 100) 
                    {
                        MessageBox.Show("El descuento no puede ser menor a 0 o mayor a 100");
                        txt_descuento.Focus();
                    }
                    if (txt_precio.Text.Trim() == string.Empty ||
                        Convert.ToDecimal(txt_descuento.Text.Trim()) < 0)
                    {
                        MessageBox.Show("El precio no puede ser menor a cero");
                        txt_precio.Focus();
                    }

                }

            }
            catch (Exception Ex)
            {
                MessageBox.Show("Error de validación" + Ex.Message.ToString(), "!!!", MessageBoxButtons.OK);

            }

            return R;

        }


        private void Limpiar()
        {
            txt_cantidad.Value = 1;
            txt_descuento.Text = "0";
            txt_precio.Text = "0";

            ObjProducto = new Logica.Producto();
        }

        private void btn_cerrar_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }

        private void dgv_lista_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dgv_lista.Rows.Count > 0 && dgv_lista.SelectedRows.Count == 1)
            {
                Limpiar();
                txt_precio.Text = dgv_lista.SelectedRows[0].Cells["C_PrecioUnitario"].Value.ToString();
            }
        }

        private void dgv_lista_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            dgv_lista.ClearSelection();
        }
    }
}
