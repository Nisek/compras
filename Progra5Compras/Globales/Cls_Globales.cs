using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Progra5Compras.Globales
{
    public static class Cls_Globales
    {
        public static Formularios.Frm_Principal miFormPrincipal = new Formularios.Frm_Principal();

        public static Logica.Usuario MiUsuarioGlobal = new Logica.Usuario();

        public static Formularios.Frm_Compra MiFormDeCompras = new Formularios.Frm_Compra();


        public const string EmailRegex =
        @"^(([\w-]+\.)+[\w-]+|([a-zA-Z]{1}|[\w-]{2,}))@"
        + @"((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?
		[0-9]{1,2}|25[0-5]|2[0-4][0-9])\."
        + @"([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?
		[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
        + @"([a-zA-Z0-9]+[\w-]+\.)+[a-zA-Z]{1}[a-zA-Z0-9-]{1,23})$";

        public static bool ValidarEmail(string email)
        {
            if (email != null)
            {
                return Regex.IsMatch(email, EmailRegex);
            }
            else
            { return false; }

        }

        //Esto me permite obtener el separador decimal del sistema 
        public static char SeparadorDecimal =
            Convert.ToChar(System.Globalization.
                CultureInfo.CurrentCulture.NumberFormat.
                NumberDecimalSeparator.ToString());

        public static bool ValidarCaracteresTexto(System.Windows.Forms.KeyPressEventArgs c,
            bool Mayusculas = false,
            bool Minisculas = false)
        {
            bool ret = false;

            if (Mayusculas)
            { c.KeyChar = char.ToUpper(c.KeyChar); }

            if (Minisculas)
            { c.KeyChar = char.ToLower(c.KeyChar); }

            if (!(char.IsLetterOrDigit(c.KeyChar)) &&
                !(char.IsPunctuation(c.KeyChar)) &&
                !(c.KeyChar == Convert.ToChar(Keys.Back)) &&
                !(c.KeyChar == Convert.ToChar(Keys.Space)) &&
                !(c.KeyChar == Convert.ToChar(Keys.Enter)))

                ret = true;
            else
                ret = false;
            return
            ret;

        }

        public static bool ValidarCaracteresNumeros(KeyPressEventArgs c,
                                             bool SoloEnteros = true)
        {
            //En el caso que presione enter acepta el valor y devuelve True
            int Asc = (int)Keys.Enter;

            if (c.KeyChar == Asc)
            {
                return true;
            }
            if (SoloEnteros == false)
            {
                if (c.KeyChar.ToString() == (".") | c.KeyChar.ToString() == (","))
                {
                    c.KeyChar = SeparadorDecimal;
                    return false;
                }
            }

            if (!(char.IsDigit(c.KeyChar)) &&
                !(c.KeyChar == Convert.ToChar(Keys.Back)) &&
                !(c.KeyChar == Convert.ToChar(Keys.Enter)))
            { return true; }
            else
            { return false; }

        }


    }
}
