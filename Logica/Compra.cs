using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace Logica
{
    public class Compra
    {

        public Compra()
        {

            MiTipoCompra = new Tipo_Compra();
            MiUsuario = new Usuario();
            MiProveedor = new Proveedor();
            ListaDetalle = new List<Compra_Detalle>();

        }
        public int Id_Compra { get; set; }
        public DateTime Fecha { get; set; }
        //Compuestos
        public Tipo_Compra MiTipoCompra { get; set; }
        public Usuario MiUsuario { get; set; }
        public Proveedor MiProveedor { get; set; }
        public List<Compra_Detalle> ListaDetalle { get; set; }


        public int Agregar() {
            int R = 0;

            Cls_Conexion MiCnn = new Cls_Conexion();

            MiCnn.ListadoDeParametros.Add(new SqlParameter("@Fecha", this.Fecha));
            MiCnn.ListadoDeParametros.Add(new SqlParameter("@IdProveedor", this.MiProveedor.Id_Proveedor));
            MiCnn.ListadoDeParametros.Add(new SqlParameter("@IdUsuario", this.MiUsuario.Id_usuario));
            MiCnn.ListadoDeParametros.Add(new SqlParameter("@IdTipoCompra", this.MiTipoCompra.ID_Tipo_Compra));
            
            Object Respuesta = MiCnn.HacerSelectEscalar("SP_Compra_Agregar_Encabezado");

            int IDRecienCreado;

            if (Respuesta != null)
            {
                try
                {
                    IDRecienCreado = Convert.ToInt32(Respuesta.ToString());

                    int Acumulador = 0;

                    if (IDRecienCreado > 0)
                    {
                        foreach (Logica.Compra_Detalle item in ListaDetalle)
                        {
                            Cls_Conexion MicnnDetalle = new Cls_Conexion();

                            MicnnDetalle.ListadoDeParametros.Add(new SqlParameter("@Id_Compra", IDRecienCreado));
                            MicnnDetalle.ListadoDeParametros.Add(new SqlParameter("@IdProducto", item.IdProducto));
                            MicnnDetalle.ListadoDeParametros.Add(new SqlParameter("@Precio", item.PrecioUnitario));
                            MicnnDetalle.ListadoDeParametros.Add(new SqlParameter("@Cantidad", item.Cantidad));
                            MicnnDetalle.ListadoDeParametros.Add(new SqlParameter("@Impuestos", item.Impuesto));
                            MicnnDetalle.ListadoDeParametros.Add(new SqlParameter("@PorcentajeDesc", item.PorcentajeDescuento));
                            MicnnDetalle.ListadoDeParametros.Add(new SqlParameter("@Descuentos", item.Descuentos));
                            MicnnDetalle.ListadoDeParametros.Add(new SqlParameter("@Total", item.TotalLinea));

                            int RespuestaIngresoDetalle = MicnnDetalle.HacerDML("SP_Detalle_Compra_Ingreso");

                            if (RespuestaIngresoDetalle > 0)
                            {
                                //que sume a un acumulador
                                Acumulador += 1;

                            }
                        }

                        if (Acumulador == ListaDetalle.Count)
                        {
                            R = IDRecienCreado;

                            this.Id_Compra = IDRecienCreado;
                        }
                    }

                }
                catch (Exception)
                {

                    throw;
                }
                

            }

            return R;
        }

        public bool Eliminar()
        {
            bool R = false;



            return R;
        }

        public Compra ConsultarPorID()
        {
            Compra R = new Compra();
            return R;
        }
        public DataTable Listar(DateTime fechaInicio, DateTime FechaFin)
        {
            DataTable R = new DataTable();

            return R;
        }
        public DataTable Listar(DateTime fechaInicio, DateTime fechaFin, int Id_proveedor){
            DataTable R = new DataTable();

            return R;
        }

        public DataTable Listar(DateTime fechaInicio, DateTime fechaFin, int tipoFactura , int Id_proveedor = 0 )
        {
            DataTable R = new DataTable();

            return R;
        }


        public CrystalDecisions.CrystalReports.Engine.ReportDocument ImprimirFactura(CrystalDecisions.CrystalReports.Engine.ReportDocument Repo)
        {
            CrystalDecisions.CrystalReports.Engine.ReportDocument Retorno = Repo;

            //se crea el objeto para crear el reporte de crystal 
            Crystal ObjCrystal = new Crystal(Retorno);

            //crear un data table con la info de la cita para asignar dicha info al reporte
            DataTable Datos = new DataTable();

            Cls_Conexion MiConexion = new Cls_Conexion();

            MiConexion.ListadoDeParametros.Add(new SqlParameter("@IdCompra", this.Id_Compra));

            DataTable dtDatos = new DataTable();

            dtDatos = MiConexion.HacerSelect("SP_Compra_Imprimir");

            if (dtDatos.Rows.Count > 0)
            {
                ObjCrystal.Datos = dtDatos;
            }

            Retorno = ObjCrystal.GenerarReporte();

            return Retorno;

        }


    }


    }

