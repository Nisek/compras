using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace Logica
{
    public class Compra_Detalle
    {
        public decimal PrecioUnitario { get; set; }
        public decimal Cantidad { get; set; }
        public decimal Impuesto { get; set; }
        public decimal PorcentajeDescuento { get; set; } = 0;
        public decimal Descuentos { get; set; } = 0;
        public decimal TotalLinea { get; set; } = 0;

        //Se crean los siguientes 2 atributos solo con la finalidad de poder "dibujar"
        //el detalle en el datagridview
        public string NombreProducto { get; set; }
        public int IdProducto { get; set; }

        public Producto MiProducto { get; set; }

        public Compra_Detalle()
        {
            MiProducto = new Producto();
        }


        public decimal TasaImpuesto()
        {
            Decimal Resultado = 0;

            DataTable R = new DataTable();

            Cls_Conexion MiCnn = new Cls_Conexion();



            MiCnn.ListadoDeParametros.Add(new SqlParameter("@IdProducto", this.IdProducto));


            R = MiCnn.HacerSelect("SP_Producto_TasaImpuesto");

            if (R != null && R.Rows.Count > 0)
            {
                Resultado = Convert.ToDecimal(R.Rows[0]["Tasa"]);
            }

            return Resultado;
        }
    }


}
