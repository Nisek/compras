using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace Logica
{
    public class Tipo_Compra
    {
        //Primero se escribe los atributos.
        public int ID_Tipo_Compra { get; set; }
        public string tipo_Compra { get; set;  }
        //luego de los atributos se escriben los metodos.

        public DataTable Listar()
        {
            DataTable ret = new DataTable();


            Cls_Conexion MICnn = new Cls_Conexion();
            ret = MICnn.HacerSelect("SP_CompraTipo_Listar");


            return ret;
        }
    }
}
