using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace Logica
{
    public class Producto
    {
        //constructor.
        public Producto() {
            MiTipoIVA = new Tipo_Iva();
            MiImpuesto = new Impuesto();
        }

        public int Id_producto { get; set; }
        public string CodigoBarras { get; set; }
        public string Nombre { get; set; }
        public int Cantidad { get; set; }
        public decimal PrecioUnitario { get; set; }
        public string Descripcion { get; set; }
        public bool eliminado { get; set; }

        // Atributos compuestos
        public Tipo_Iva MiTipoIVA { get; set; }
        public Impuesto MiImpuesto { get; set; }




        public Producto Agregar() {
            Producto R = new Producto();

            return R;
        }
        public bool Modificar() {
            bool R = false;



            return R;
        }
        public bool Eliminar()
        {
            bool R = false;



            return R;
        }
        public Producto ConsultarPorID() {
            Producto R = new Producto();
            return R;
        }

        public DataTable Listar( bool Activos = true) {
            DataTable R = new DataTable();

            Cls_Conexion MiCnn = new Cls_Conexion();



            MiCnn.ListadoDeParametros.Add(new SqlParameter("@Activos", Activos));


            R = MiCnn.HacerSelect("SP_Producto_Listar");

            return R;
        }
        public DataTable ListarConFiltro(string Filtro) {
            DataTable R = new DataTable();
            return R;
        }
    }
}
