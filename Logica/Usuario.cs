using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace Logica
{
    public class Usuario
    {
        public Usuario()
        {
            // este es el constructor basico de la clase.
            // aca debemos instanciar los atributos compuestos.
            TipoUsuario = new Usuario_tipo();
        }

        public int Id_usuario { get; set; }
        public string Nombre { get; set; }
        public string Cedula { get; set; }
        public string UsuarioNombre { get; set; }
        public string Contra { get; set; }
        public string Email { get; set; }
        public bool Eliminado { get; set; }

        //se establece el atributo compuesto del tipo usuario... se puede ver como la aplicacion de un FK
        public Usuario_tipo TipoUsuario { get; set; }

        //metodo constructor




        //metodos de la clase
        public bool Agregar() {
            bool R = false;

            Cls_Conexion MiCnn = new Cls_Conexion();

            MiCnn.ListadoDeParametros.Add(new SqlParameter("@Nombre", this.Nombre));
            MiCnn.ListadoDeParametros.Add(new SqlParameter("@Cedula", this.Cedula));
            MiCnn.ListadoDeParametros.Add(new SqlParameter("@UsuarioNombre", this.UsuarioNombre));

            //Antes de enviar la contraseña a la base de datos primero se encripta
            Cls_Encriptador MiEncriptador = new Cls_Encriptador();
            string ContraEncriptada = MiEncriptador.Encriptar(this.Contra);
            MiCnn.ListadoDeParametros.Add(new SqlParameter("@Contra", ContraEncriptada));


            MiCnn.ListadoDeParametros.Add(new SqlParameter("@Email", this.Email));
            MiCnn.ListadoDeParametros.Add(new SqlParameter("@FK_UsuarioTipo", this.TipoUsuario.Id_usuario_tipo));

            int Respuesta = MiCnn.HacerDML("SP_Usuario_Agregar");

            if (Respuesta > 0)
            {
                R = true;
            }

            return R;
        }

        public bool Modificar()
        {
            bool R = false;

            Cls_Conexion MiCnn = new Cls_Conexion();

            MiCnn.ListadoDeParametros.Add(new SqlParameter("@ID", this.Id_usuario));

            MiCnn.ListadoDeParametros.Add(new SqlParameter("@Nombre", this.Nombre));
            MiCnn.ListadoDeParametros.Add(new SqlParameter("@Cedula", this.Cedula));
            MiCnn.ListadoDeParametros.Add(new SqlParameter("@UsuarioNombre", this.UsuarioNombre));


            //Si el atributo de Contraseña llega con datos, se procede a encriptar, sino No.
            //Antes de enviar la contraseña a la base de datos primero se encripta
            string ContraEncriptada = "";
            if (this.Contra != string.Empty)
            {
                Cls_Encriptador MiEncriptador = new Cls_Encriptador();
                ContraEncriptada = MiEncriptador.Encriptar(this.Contra);
            }
            MiCnn.ListadoDeParametros.Add(new SqlParameter("@Contra", ContraEncriptada));


            MiCnn.ListadoDeParametros.Add(new SqlParameter("@Email", this.Email));
            MiCnn.ListadoDeParametros.Add(new SqlParameter("@FK_UsuarioTipo", this.TipoUsuario.Id_usuario_tipo));

            int Respuesta = MiCnn.HacerDML("SP_Usuario_Modificar");

            if (Respuesta > 0)
            {
                R = true;
            }

            return R;
        }

        public bool EliminarActivar(bool eliminar)
        {
            bool R = false;

            Cls_Conexion MiCnn = new Cls_Conexion();

            MiCnn.ListadoDeParametros.Add(new SqlParameter("@ID", this.Id_usuario));
            MiCnn.ListadoDeParametros.Add(new SqlParameter("@Eliminar", eliminar));


            int Respuesta = MiCnn.HacerDML("SP_Usuario_ActivarDesactivar");

            if (Respuesta > 0)
            {
                R = true;
            }

            return R;
        }

        public bool ConsultarPorID()
        {
            bool R = false;

            Cls_Conexion MiCnn = new Cls_Conexion();
            MiCnn.ListadoDeParametros.Add(new SqlParameter("@ID", this.Id_usuario));

            DataTable Respuesta = new DataTable();
            Respuesta = MiCnn.HacerSelect("SP_Usuario_ConsultarPorID");

            if (Respuesta != null && Respuesta.Rows.Count > 0)
            {
                R = true;
            }

            return R;
        }
        public bool ConsultarPorCedula()
        {
            bool R = false;

            Cls_Conexion MiCnn = new Cls_Conexion();
            MiCnn.ListadoDeParametros.Add(new SqlParameter("@Cedula", this.Cedula));

            DataTable Respuesta = new DataTable();
            Respuesta = MiCnn.HacerSelect("SP_Usuario_ConsultarPorCedula");

            if (Respuesta != null && Respuesta.Rows.Count > 0)
            {
                R = true;
            }

            return R;
        }
        public bool ConsultarPorUserName()
        {
            bool R = false;

            Cls_Conexion MiCnn = new Cls_Conexion();
            MiCnn.ListadoDeParametros.Add(new SqlParameter("@UsuarioNombre", this.UsuarioNombre));

            DataTable Respuesta = new DataTable();
            Respuesta = MiCnn.HacerSelect("SP_Usuario_ConsultarPorUsuarioNombre");

            if (Respuesta != null && Respuesta.Rows.Count > 0)
            {
                R = true;
            }



            return R;
        }
        public DataTable Listar(bool Activos = true ) {

            DataTable R = new DataTable();

            //1.3.1 y 1.3.2
            Cls_Conexion MiCnn = new Cls_Conexion();

            //Se deben agregar los parametros requeridos 
            //por el SP. En este Caso @Activos

            MiCnn.ListadoDeParametros.Add(new SqlParameter("@Activos", Activos));

            //1.3.3 y 1.3.4
            R = MiCnn.HacerSelect("SP_Usuario_Listar");

            
            return R;
        }
        public DataTable ListarConFiltro(String Filtro, bool VerEliminados) {
            DataTable R = new DataTable();

            Cls_Conexion MiCnn = new Cls_Conexion();

            MiCnn.ListadoDeParametros.Add(new SqlParameter("@Eliminados", VerEliminados));
            MiCnn.ListadoDeParametros.Add(new SqlParameter("@Filtro", Filtro));


            R = MiCnn.HacerSelect("SP_Usuario_ListarConFiltro");

            return R;
        }

        public Usuario ValidarUsuario()
        {
            Usuario R = new Usuario();

            DataTable DtUsuario = new DataTable();

            Cls_Conexion MiCnn = new Cls_Conexion();

            MiCnn.ListadoDeParametros.Add(new SqlParameter("@UsuarioNombre", this.UsuarioNombre));

            string PassEncriptado;
            Cls_Encriptador ObjEncriptador = new Cls_Encriptador();
            PassEncriptado = ObjEncriptador.Encriptar(this.Contra);
            MiCnn.ListadoDeParametros.Add(new SqlParameter("@Contra", PassEncriptado));


            DtUsuario = MiCnn.HacerSelect("SP_Usuario_ValidarIngreso");

            if (DtUsuario != null && DtUsuario.Rows.Count == 1)
            {
                DataRow MiFIla = DtUsuario.Rows[0];

                R.Id_usuario = Convert.ToInt32(MiFIla["Id_usuario"]);
                R.Nombre = MiFIla["Nombre"].ToString();
                R.Cedula = MiFIla["Cedula"].ToString();
                R.UsuarioNombre = MiFIla["UsuarioNombre"].ToString();
                R.Contra = "QueTeImporta";
                R.Email = MiFIla["Email"].ToString();
                R.Eliminado = Convert.ToBoolean(MiFIla["Eliminado"]);
                R.TipoUsuario.Id_usuario_tipo = Convert.ToInt32(MiFIla["Id_usuarioTipo"]);
                R.TipoUsuario.Tipo = MiFIla["tipo"].ToString();
            }   

            return R;
        }
    }
}
