using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica
{
    public class Crystal
    {
        public CrystalDecisions.CrystalReports.Engine.ReportDocument Reporte { get; set; }

        public DataTable Datos { get; set; }

        public Crystal()
        {
        }

        public Crystal(CrystalDecisions.CrystalReports.Engine.ReportDocument Rpt)
        {
            Reporte = Rpt;
        }

        public CrystalDecisions.CrystalReports.Engine.ReportDocument GenerarReporte()
        {
            if (Datos.Rows.Count > 0)
            {
                //la fuente de datos puede ser asignada al reporte 
                Reporte.SetDataSource(Datos);

                //una vez asignada la fuente de datos se procede a enviarlo al visualizador
                return Reporte;
            }
            else
            { return null; }

        }
    }
}
