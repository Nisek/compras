using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
namespace Logica
{
    public class Proveedor
    {

        public int Id_Proveedor { get; set;  }
        public string Cedula { get; set; }
        public string Nombre { get; set; }
        public string Telefono { get; set; }
        public string Direccion { get; set; }
        public string Email { get; set; }
        public bool Eliminado { get; set; }

        public Proveedor Agregar() {
            Proveedor R = new Proveedor();

            return R;
        }

        public bool Modificar() {
            bool R = false;



            return R;
        }

        public bool Eliminar() {
            bool R = false;



            return R;
        }

        public Proveedor ConsutarPorID() {

            Proveedor R = new Proveedor();

            return R;

        }

        public Proveedor ConsultarPorCedula() {
            Proveedor R = new Proveedor();

            return R;


        }

        public DataTable Listar(bool Activos = true) {
            DataTable R = new DataTable();

            Cls_Conexion MiCnn = new Cls_Conexion();

      

            MiCnn.ListadoDeParametros.Add(new SqlParameter("@Activos", Activos));

      
            R = MiCnn.HacerSelect("SP_Proveedor_Listar");

            return R;


        }
        public DataTable ListarConFiltro(String Filtro) {
            DataTable R = new DataTable();
            return R;

        }

    }
}
